﻿using RuelSkype.code;
namespace RuelSkype.ui
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.users_count = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.close = new RuelSkype.code.RuElButton();
            this.name = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.log = new RuelSkype.code.MultiLineListBox();
            this.users_table = new System.Windows.Forms.DataGridView();
            this.table_select = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.table_login = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_full_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_is_friend = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_is_online = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_contacts = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_lang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_country = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_region = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_city = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_last_visit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_birthday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_home_page = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_status_text = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_about_user = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_home_phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_office_phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_mobile_phone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.selector_actions = new System.Windows.Forms.TabControl();
            this.step_one = new System.Windows.Forms.TabPage();
            this.dont_clear = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.load_users = new RuelSkype.code.RuElButton();
            this.from_categorys = new System.Windows.Forms.RadioButton();
            this.from_csv = new System.Windows.Forms.RadioButton();
            this.from_chats = new System.Windows.Forms.RadioButton();
            this.from_contacts = new System.Windows.Forms.RadioButton();
            this.from_search = new System.Windows.Forms.RadioButton();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.languages = new System.Windows.Forms.ComboBox();
            this.countries = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gender_male = new System.Windows.Forms.RadioButton();
            this.gender_female = new System.Windows.Forms.RadioButton();
            this.gender_shemale = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.is_friend = new System.Windows.Forms.RadioButton();
            this.not_friend = new System.Windows.Forms.RadioButton();
            this.all_statuses = new System.Windows.Forms.RadioButton();
            this.append_filters = new RuelSkype.code.RuElButton();
            this.contacts_count_end = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.contacts_count_start = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.age_end = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.age_start = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.by_country = new System.Windows.Forms.CheckBox();
            this.by_status = new System.Windows.Forms.CheckBox();
            this.by_contacts_count = new System.Windows.Forms.CheckBox();
            this.by_lang = new System.Windows.Forms.CheckBox();
            this.by_age = new System.Windows.Forms.CheckBox();
            this.is_birthday = new System.Windows.Forms.CheckBox();
            this.by_gender = new System.Windows.Forms.CheckBox();
            this.is_online = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.filter = new System.Windows.Forms.RadioButton();
            this.unselect_all = new System.Windows.Forms.RadioButton();
            this.all_users = new System.Windows.Forms.RadioButton();
            this.step_three = new System.Windows.Forms.TabPage();
            this.delete_contacts = new System.Windows.Forms.RadioButton();
            this.append_action = new RuelSkype.code.RuElButton();
            this.save_to_csv = new System.Windows.Forms.RadioButton();
            this.add_to_category = new System.Windows.Forms.RadioButton();
            this.send_sms = new System.Windows.Forms.RadioButton();
            this.add_to_contacts = new System.Windows.Forms.RadioButton();
            this.send_message = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.minimize = new RuelSkype.code.RuElButton();
            this.message = new System.Windows.Forms.RichTextBox();
            this.fast_search = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.version = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.users_table)).BeginInit();
            this.selector_actions.SuspendLayout();
            this.step_one.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.step_three.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            resources.ApplyResources(this.notifyIcon1, "notifyIcon1");
            // 
            // users_count
            // 
            this.users_count.BackColor = System.Drawing.Color.Transparent;
            this.users_count.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.users_count, "users_count");
            this.users_count.Name = "users_count";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // close
            // 
            this.close.BackColor = System.Drawing.Color.Transparent;
            this.close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.close.FlatAppearance.BorderSize = 0;
            this.close.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.close.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.close, "close");
            this.close.Name = "close";
            this.close.UseVisualStyleBackColor = false;
            this.close.Click += new System.EventHandler(this.close_Click_1);
            // 
            // name
            // 
            this.name.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.name, "name");
            this.name.ForeColor = System.Drawing.Color.White;
            this.name.Name = "name";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // log
            // 
            this.log.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.log.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.log.FormattingEnabled = true;
            resources.ApplyResources(this.log, "log");
            this.log.Name = "log";
            // 
            // users_table
            // 
            this.users_table.AllowUserToAddRows = false;
            this.users_table.AllowUserToDeleteRows = false;
            this.users_table.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.users_table.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.users_table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.users_table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.table_select,
            this.table_login,
            this.table_full_name,
            this.table_is_friend,
            this.table_is_online,
            this.table_contacts,
            this.table_gender,
            this.table_lang,
            this.table_country,
            this.table_region,
            this.table_city,
            this.table_last_visit,
            this.table_birthday,
            this.table_age,
            this.table_home_page,
            this.table_status_text,
            this.table_about_user,
            this.table_home_phone,
            this.table_office_phone,
            this.table_mobile_phone});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.users_table.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.users_table, "users_table");
            this.users_table.Name = "users_table";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.users_table.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.users_table.RowHeadersVisible = false;
            this.users_table.RowTemplate.Height = 24;
            this.users_table.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.users_table_CellContentClick);
            this.users_table.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.users_table_CellMouseClick_1);
            this.users_table.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.users_table_CellMouseUp);
            this.users_table.MouseClick += new System.Windows.Forms.MouseEventHandler(this.users_table_MouseClick);
            // 
            // table_select
            // 
            this.table_select.DataPropertyName = "selected";
            resources.ApplyResources(this.table_select, "table_select");
            this.table_select.Name = "table_select";
            this.table_select.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.table_select.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // table_login
            // 
            this.table_login.DataPropertyName = "login";
            resources.ApplyResources(this.table_login, "table_login");
            this.table_login.Name = "table_login";
            this.table_login.ReadOnly = true;
            // 
            // table_full_name
            // 
            this.table_full_name.DataPropertyName = "full_name";
            resources.ApplyResources(this.table_full_name, "table_full_name");
            this.table_full_name.Name = "table_full_name";
            // 
            // table_is_friend
            // 
            this.table_is_friend.DataPropertyName = "is_friend";
            resources.ApplyResources(this.table_is_friend, "table_is_friend");
            this.table_is_friend.Name = "table_is_friend";
            // 
            // table_is_online
            // 
            this.table_is_online.DataPropertyName = "is_online";
            resources.ApplyResources(this.table_is_online, "table_is_online");
            this.table_is_online.Name = "table_is_online";
            this.table_is_online.ReadOnly = true;
            // 
            // table_contacts
            // 
            this.table_contacts.DataPropertyName = "contacts_count";
            resources.ApplyResources(this.table_contacts, "table_contacts");
            this.table_contacts.Name = "table_contacts";
            this.table_contacts.ReadOnly = true;
            // 
            // table_gender
            // 
            this.table_gender.DataPropertyName = "gender";
            resources.ApplyResources(this.table_gender, "table_gender");
            this.table_gender.Name = "table_gender";
            this.table_gender.ReadOnly = true;
            // 
            // table_lang
            // 
            this.table_lang.DataPropertyName = "language";
            resources.ApplyResources(this.table_lang, "table_lang");
            this.table_lang.Name = "table_lang";
            this.table_lang.ReadOnly = true;
            // 
            // table_country
            // 
            this.table_country.DataPropertyName = "country";
            resources.ApplyResources(this.table_country, "table_country");
            this.table_country.Name = "table_country";
            this.table_country.ReadOnly = true;
            // 
            // table_region
            // 
            this.table_region.DataPropertyName = "province";
            resources.ApplyResources(this.table_region, "table_region");
            this.table_region.Name = "table_region";
            this.table_region.ReadOnly = true;
            // 
            // table_city
            // 
            this.table_city.DataPropertyName = "city";
            resources.ApplyResources(this.table_city, "table_city");
            this.table_city.Name = "table_city";
            this.table_city.ReadOnly = true;
            // 
            // table_last_visit
            // 
            this.table_last_visit.DataPropertyName = "last_visit";
            resources.ApplyResources(this.table_last_visit, "table_last_visit");
            this.table_last_visit.Name = "table_last_visit";
            this.table_last_visit.ReadOnly = true;
            // 
            // table_birthday
            // 
            this.table_birthday.DataPropertyName = "birthday";
            resources.ApplyResources(this.table_birthday, "table_birthday");
            this.table_birthday.Name = "table_birthday";
            this.table_birthday.ReadOnly = true;
            // 
            // table_age
            // 
            this.table_age.DataPropertyName = "age";
            resources.ApplyResources(this.table_age, "table_age");
            this.table_age.Name = "table_age";
            // 
            // table_home_page
            // 
            this.table_home_page.DataPropertyName = "home_page";
            resources.ApplyResources(this.table_home_page, "table_home_page");
            this.table_home_page.Name = "table_home_page";
            this.table_home_page.ReadOnly = true;
            // 
            // table_status_text
            // 
            this.table_status_text.DataPropertyName = "status_text";
            resources.ApplyResources(this.table_status_text, "table_status_text");
            this.table_status_text.Name = "table_status_text";
            this.table_status_text.ReadOnly = true;
            // 
            // table_about_user
            // 
            this.table_about_user.DataPropertyName = "about_user";
            resources.ApplyResources(this.table_about_user, "table_about_user");
            this.table_about_user.Name = "table_about_user";
            this.table_about_user.ReadOnly = true;
            // 
            // table_home_phone
            // 
            this.table_home_phone.DataPropertyName = "phone_home";
            resources.ApplyResources(this.table_home_phone, "table_home_phone");
            this.table_home_phone.Name = "table_home_phone";
            this.table_home_phone.ReadOnly = true;
            // 
            // table_office_phone
            // 
            this.table_office_phone.DataPropertyName = "phone_office";
            resources.ApplyResources(this.table_office_phone, "table_office_phone");
            this.table_office_phone.Name = "table_office_phone";
            this.table_office_phone.ReadOnly = true;
            // 
            // table_mobile_phone
            // 
            this.table_mobile_phone.DataPropertyName = "phone_mobile";
            resources.ApplyResources(this.table_mobile_phone, "table_mobile_phone");
            this.table_mobile_phone.Name = "table_mobile_phone";
            this.table_mobile_phone.ReadOnly = true;
            // 
            // selector_actions
            // 
            resources.ApplyResources(this.selector_actions, "selector_actions");
            this.selector_actions.Controls.Add(this.step_one);
            this.selector_actions.Controls.Add(this.tabPage1);
            this.selector_actions.Controls.Add(this.step_three);
            this.selector_actions.Name = "selector_actions";
            this.selector_actions.SelectedIndex = 0;
            // 
            // step_one
            // 
            this.step_one.BackColor = System.Drawing.SystemColors.Control;
            this.step_one.Controls.Add(this.dont_clear);
            this.step_one.Controls.Add(this.label11);
            this.step_one.Controls.Add(this.load_users);
            this.step_one.Controls.Add(this.from_categorys);
            this.step_one.Controls.Add(this.from_csv);
            this.step_one.Controls.Add(this.from_chats);
            this.step_one.Controls.Add(this.from_contacts);
            this.step_one.Controls.Add(this.from_search);
            resources.ApplyResources(this.step_one, "step_one");
            this.step_one.Name = "step_one";
            // 
            // dont_clear
            // 
            resources.ApplyResources(this.dont_clear, "dont_clear");
            this.dont_clear.Name = "dont_clear";
            this.dont_clear.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // load_users
            // 
            this.load_users.BackgroundImage = global::RuelSkype.code.Properties.Resources.button;
            this.load_users.Cursor = System.Windows.Forms.Cursors.Hand;
            this.load_users.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.load_users, "load_users");
            this.load_users.Name = "load_users";
            this.load_users.UseVisualStyleBackColor = true;
            this.load_users.Click += new System.EventHandler(this.load_users_Click);
            // 
            // from_categorys
            // 
            resources.ApplyResources(this.from_categorys, "from_categorys");
            this.from_categorys.Name = "from_categorys";
            this.from_categorys.UseVisualStyleBackColor = true;
            // 
            // from_csv
            // 
            resources.ApplyResources(this.from_csv, "from_csv");
            this.from_csv.Name = "from_csv";
            this.from_csv.UseVisualStyleBackColor = true;
            // 
            // from_chats
            // 
            resources.ApplyResources(this.from_chats, "from_chats");
            this.from_chats.Name = "from_chats";
            this.from_chats.UseVisualStyleBackColor = true;
            // 
            // from_contacts
            // 
            resources.ApplyResources(this.from_contacts, "from_contacts");
            this.from_contacts.Name = "from_contacts";
            this.from_contacts.UseVisualStyleBackColor = true;
            // 
            // from_search
            // 
            resources.ApplyResources(this.from_search, "from_search");
            this.from_search.Checked = true;
            this.from_search.Name = "from_search";
            this.from_search.TabStop = true;
            this.from_search.UseVisualStyleBackColor = true;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.languages);
            this.tabPage1.Controls.Add(this.countries);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.append_filters);
            this.tabPage1.Controls.Add(this.contacts_count_end);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.contacts_count_start);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.age_end);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.age_start);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.by_country);
            this.tabPage1.Controls.Add(this.by_status);
            this.tabPage1.Controls.Add(this.by_contacts_count);
            this.tabPage1.Controls.Add(this.by_lang);
            this.tabPage1.Controls.Add(this.by_age);
            this.tabPage1.Controls.Add(this.is_birthday);
            this.tabPage1.Controls.Add(this.by_gender);
            this.tabPage1.Controls.Add(this.is_online);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.filter);
            this.tabPage1.Controls.Add(this.unselect_all);
            this.tabPage1.Controls.Add(this.all_users);
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.Name = "tabPage1";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // languages
            // 
            this.languages.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.languages.FormattingEnabled = true;
            resources.ApplyResources(this.languages, "languages");
            this.languages.Name = "languages";
            // 
            // countries
            // 
            this.countries.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.countries.FormattingEnabled = true;
            resources.ApplyResources(this.countries, "countries");
            this.countries.Name = "countries";
            this.countries.Sorted = true;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.gender_male);
            this.panel2.Controls.Add(this.gender_female);
            this.panel2.Controls.Add(this.gender_shemale);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // gender_male
            // 
            resources.ApplyResources(this.gender_male, "gender_male");
            this.gender_male.Name = "gender_male";
            this.gender_male.TabStop = true;
            this.gender_male.UseVisualStyleBackColor = true;
            // 
            // gender_female
            // 
            resources.ApplyResources(this.gender_female, "gender_female");
            this.gender_female.Name = "gender_female";
            this.gender_female.TabStop = true;
            this.gender_female.UseVisualStyleBackColor = true;
            // 
            // gender_shemale
            // 
            resources.ApplyResources(this.gender_shemale, "gender_shemale");
            this.gender_shemale.Name = "gender_shemale";
            this.gender_shemale.TabStop = true;
            this.gender_shemale.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.is_friend);
            this.panel1.Controls.Add(this.not_friend);
            this.panel1.Controls.Add(this.all_statuses);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // is_friend
            // 
            resources.ApplyResources(this.is_friend, "is_friend");
            this.is_friend.Name = "is_friend";
            this.is_friend.TabStop = true;
            this.is_friend.UseVisualStyleBackColor = true;
            // 
            // not_friend
            // 
            resources.ApplyResources(this.not_friend, "not_friend");
            this.not_friend.Name = "not_friend";
            this.not_friend.TabStop = true;
            this.not_friend.UseVisualStyleBackColor = true;
            // 
            // all_statuses
            // 
            resources.ApplyResources(this.all_statuses, "all_statuses");
            this.all_statuses.Name = "all_statuses";
            this.all_statuses.TabStop = true;
            this.all_statuses.UseVisualStyleBackColor = true;
            // 
            // append_filters
            // 
            this.append_filters.BackgroundImage = global::RuelSkype.code.Properties.Resources.button;
            this.append_filters.Cursor = System.Windows.Forms.Cursors.Hand;
            this.append_filters.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.append_filters, "append_filters");
            this.append_filters.Name = "append_filters";
            this.append_filters.UseVisualStyleBackColor = true;
            this.append_filters.Click += new System.EventHandler(this.append_filters_Click);
            // 
            // contacts_count_end
            // 
            resources.ApplyResources(this.contacts_count_end, "contacts_count_end");
            this.contacts_count_end.Name = "contacts_count_end";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // contacts_count_start
            // 
            resources.ApplyResources(this.contacts_count_start, "contacts_count_start");
            this.contacts_count_start.Name = "contacts_count_start";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // age_end
            // 
            resources.ApplyResources(this.age_end, "age_end");
            this.age_end.Name = "age_end";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // age_start
            // 
            resources.ApplyResources(this.age_start, "age_start");
            this.age_start.Name = "age_start";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // by_country
            // 
            resources.ApplyResources(this.by_country, "by_country");
            this.by_country.Name = "by_country";
            this.by_country.UseVisualStyleBackColor = true;
            // 
            // by_status
            // 
            resources.ApplyResources(this.by_status, "by_status");
            this.by_status.Name = "by_status";
            this.by_status.UseVisualStyleBackColor = true;
            // 
            // by_contacts_count
            // 
            resources.ApplyResources(this.by_contacts_count, "by_contacts_count");
            this.by_contacts_count.Name = "by_contacts_count";
            this.by_contacts_count.UseVisualStyleBackColor = true;
            // 
            // by_lang
            // 
            resources.ApplyResources(this.by_lang, "by_lang");
            this.by_lang.Name = "by_lang";
            this.by_lang.UseVisualStyleBackColor = true;
            // 
            // by_age
            // 
            resources.ApplyResources(this.by_age, "by_age");
            this.by_age.Name = "by_age";
            this.by_age.UseVisualStyleBackColor = true;
            // 
            // is_birthday
            // 
            resources.ApplyResources(this.is_birthday, "is_birthday");
            this.is_birthday.Name = "is_birthday";
            this.is_birthday.UseVisualStyleBackColor = true;
            // 
            // by_gender
            // 
            resources.ApplyResources(this.by_gender, "by_gender");
            this.by_gender.Name = "by_gender";
            this.by_gender.UseVisualStyleBackColor = true;
            // 
            // is_online
            // 
            resources.ApplyResources(this.is_online, "is_online");
            this.is_online.Name = "is_online";
            this.is_online.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.Image = global::RuelSkype.code.Properties.Resources.line;
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // filter
            // 
            resources.ApplyResources(this.filter, "filter");
            this.filter.Name = "filter";
            this.filter.UseVisualStyleBackColor = true;
            // 
            // unselect_all
            // 
            resources.ApplyResources(this.unselect_all, "unselect_all");
            this.unselect_all.Name = "unselect_all";
            this.unselect_all.UseVisualStyleBackColor = true;
            // 
            // all_users
            // 
            resources.ApplyResources(this.all_users, "all_users");
            this.all_users.Checked = true;
            this.all_users.Name = "all_users";
            this.all_users.TabStop = true;
            this.all_users.UseVisualStyleBackColor = true;
            // 
            // step_three
            // 
            this.step_three.Controls.Add(this.delete_contacts);
            this.step_three.Controls.Add(this.append_action);
            this.step_three.Controls.Add(this.save_to_csv);
            this.step_three.Controls.Add(this.add_to_category);
            this.step_three.Controls.Add(this.send_sms);
            this.step_three.Controls.Add(this.add_to_contacts);
            this.step_three.Controls.Add(this.send_message);
            this.step_three.Controls.Add(this.label9);
            resources.ApplyResources(this.step_three, "step_three");
            this.step_three.Name = "step_three";
            this.step_three.UseVisualStyleBackColor = true;
            // 
            // delete_contacts
            // 
            resources.ApplyResources(this.delete_contacts, "delete_contacts");
            this.delete_contacts.Name = "delete_contacts";
            this.delete_contacts.TabStop = true;
            this.delete_contacts.UseVisualStyleBackColor = true;
            // 
            // append_action
            // 
            this.append_action.BackgroundImage = global::RuelSkype.code.Properties.Resources.button;
            this.append_action.Cursor = System.Windows.Forms.Cursors.Hand;
            this.append_action.FlatAppearance.BorderSize = 0;
            this.append_action.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.append_action.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.append_action, "append_action");
            this.append_action.Name = "append_action";
            this.append_action.UseVisualStyleBackColor = true;
            this.append_action.Click += new System.EventHandler(this.append_action_Click);
            // 
            // save_to_csv
            // 
            resources.ApplyResources(this.save_to_csv, "save_to_csv");
            this.save_to_csv.Name = "save_to_csv";
            this.save_to_csv.UseVisualStyleBackColor = true;
            // 
            // add_to_category
            // 
            resources.ApplyResources(this.add_to_category, "add_to_category");
            this.add_to_category.Name = "add_to_category";
            this.add_to_category.UseVisualStyleBackColor = true;
            // 
            // send_sms
            // 
            resources.ApplyResources(this.send_sms, "send_sms");
            this.send_sms.Name = "send_sms";
            this.send_sms.UseVisualStyleBackColor = true;
            // 
            // add_to_contacts
            // 
            resources.ApplyResources(this.add_to_contacts, "add_to_contacts");
            this.add_to_contacts.Name = "add_to_contacts";
            this.add_to_contacts.UseVisualStyleBackColor = true;
            // 
            // send_message
            // 
            resources.ApplyResources(this.send_message, "send_message");
            this.send_message.Checked = true;
            this.send_message.Name = "send_message";
            this.send_message.TabStop = true;
            this.send_message.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // minimize
            // 
            this.minimize.BackColor = System.Drawing.Color.Transparent;
            this.minimize.BackgroundImage = global::RuelSkype.code.Properties.Resources.minimize;
            this.minimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.minimize.FlatAppearance.BorderSize = 0;
            this.minimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.minimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.minimize, "minimize");
            this.minimize.Name = "minimize";
            this.minimize.UseVisualStyleBackColor = false;
            this.minimize.Click += new System.EventHandler(this.minimize_Click);
            // 
            // message
            // 
            this.message.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.message, "message");
            this.message.Name = "message";
            // 
            // fast_search
            // 
            this.fast_search.ForeColor = System.Drawing.SystemColors.InactiveBorder;
            resources.ApplyResources(this.fast_search, "fast_search");
            this.fast_search.Name = "fast_search";
            this.fast_search.Click += new System.EventHandler(this.fast_search_Click);
            this.fast_search.KeyUp += new System.Windows.Forms.KeyEventHandler(this.fast_search_KeyUp);
            this.fast_search.Leave += new System.EventHandler(this.fast_search_Leave);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // version
            // 
            resources.ApplyResources(this.version, "version");
            this.version.BackColor = System.Drawing.Color.Transparent;
            this.version.Name = "version";
            // 
            // MainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::RuelSkype.code.Properties.Resources.background;
            this.CausesValidation = false;
            this.Controls.Add(this.version);
            this.Controls.Add(this.fast_search);
            this.Controls.Add(this.users_count);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.close);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.log);
            this.Controls.Add(this.users_table);
            this.Controls.Add(this.selector_actions);
            this.Controls.Add(this.minimize);
            this.Controls.Add(this.message);
            this.Controls.Add(this.label12);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.users_table)).EndInit();
            this.selector_actions.ResumeLayout(false);
            this.step_one.ResumeLayout(false);
            this.step_one.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.step_three.ResumeLayout(false);
            this.step_three.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private RuelSkype.code.RuElButton minimize;
        private System.Windows.Forms.TabControl selector_actions;
        private System.Windows.Forms.TabPage step_one;
        private RuelSkype.code.RuElButton load_users;
        private System.Windows.Forms.RadioButton from_categorys;
        private System.Windows.Forms.RadioButton from_csv;
        private System.Windows.Forms.RadioButton from_chats;
        private System.Windows.Forms.RadioButton from_contacts;
        private System.Windows.Forms.RadioButton from_search;
        private System.Windows.Forms.DataGridView users_table;
        private MultiLineListBox log;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.CheckBox by_country;
        private System.Windows.Forms.CheckBox by_status;
        private System.Windows.Forms.CheckBox by_contacts_count;
        private System.Windows.Forms.CheckBox by_lang;
        private System.Windows.Forms.CheckBox by_age;
        private System.Windows.Forms.CheckBox is_birthday;
        private System.Windows.Forms.CheckBox by_gender;
        private System.Windows.Forms.CheckBox is_online;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton filter;
        private System.Windows.Forms.RadioButton unselect_all;
        private System.Windows.Forms.RadioButton all_users;
        private System.Windows.Forms.TextBox contacts_count_end;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox contacts_count_start;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox age_end;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox age_start;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton is_friend;
        private System.Windows.Forms.RadioButton not_friend;
        private System.Windows.Forms.RadioButton all_statuses;
        private System.Windows.Forms.RadioButton gender_female;
        private System.Windows.Forms.RadioButton gender_male;
        private System.Windows.Forms.RadioButton gender_shemale;
        private System.Windows.Forms.TabPage step_three;
        private RuelSkype.code.RuElButton append_action;
        private System.Windows.Forms.RadioButton save_to_csv;
        private System.Windows.Forms.RadioButton add_to_category;
        private System.Windows.Forms.RadioButton send_sms;
        private System.Windows.Forms.RadioButton add_to_contacts;
        private System.Windows.Forms.RadioButton send_message;
        private System.Windows.Forms.Label label9;
        private RuElButton append_filters;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox message;
        private System.Windows.Forms.ComboBox countries;
        private System.Windows.Forms.ComboBox languages;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox dont_clear;
        private System.Windows.Forms.Label users_count;
        private RuelSkype.code.RuElButton close;
        private System.Windows.Forms.DataGridViewCheckBoxColumn table_select;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_login;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_full_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_is_friend;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_is_online;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_contacts;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_lang;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_country;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_region;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_city;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_last_visit;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_birthday;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_age;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_home_page;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_status_text;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_about_user;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_home_phone;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_office_phone;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_mobile_phone;
        private System.Windows.Forms.TextBox fast_search;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton delete_contacts;
        private System.Windows.Forms.Label version;
    }
}

