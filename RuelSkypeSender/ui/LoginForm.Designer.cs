﻿using RuelSkype.code;
namespace RuelSkype.ui
{
    partial class LoginForm:RuElForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.remember_me = new System.Windows.Forms.CheckBox();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.error = new System.Windows.Forms.Label();
            this.close = new RuelSkype.code.RuElButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ruelsoft_login_action = new RuelSkype.code.RuElButton();
            this.ruelsoft_password = new System.Windows.Forms.TextBox();
            this.ruelsoft_login = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // remember_me
            // 
            this.remember_me.BackColor = System.Drawing.Color.Transparent;
            this.remember_me.Font = new System.Drawing.Font("Tahoma", 7.1F, System.Drawing.FontStyle.Bold);
            this.remember_me.ForeColor = System.Drawing.Color.White;
            this.remember_me.Location = new System.Drawing.Point(195, 126);
            this.remember_me.Name = "remember_me";
            this.remember_me.Size = new System.Drawing.Size(151, 24);
            this.remember_me.TabIndex = 11;
            this.remember_me.Text = "Запомнить меня";
            this.remember_me.UseVisualStyleBackColor = false;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel2.Location = new System.Drawing.Point(134, 192);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(89, 13);
            this.linkLabel2.TabIndex = 10;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Забыли пароль?";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Location = new System.Drawing.Point(9, 192);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(94, 20);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Регистрация";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(434, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Внимание! Перед вводом логина и пароля - откройте сначала Ваш Скайп!";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(12, 226);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(543, 39);
            this.label3.TabIndex = 7;
            this.label3.Text = "Внимание! Когда нажмёте \"Войти\" - пройдите в Ваш Скайп и нажмите ДАТЬ ДОСТУП!";
            // 
            // error
            // 
            this.error.BackColor = System.Drawing.Color.Transparent;
            this.error.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.error.ForeColor = System.Drawing.Color.Red;
            this.error.Location = new System.Drawing.Point(352, 126);
            this.error.Name = "error";
            this.error.Size = new System.Drawing.Size(228, 100);
            this.error.TabIndex = 6;
            // 
            // close
            // 
            this.close.BackColor = System.Drawing.Color.Transparent;
            this.close.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("close.BackgroundImage")));
            this.close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.close.FlatAppearance.BorderSize = 0;
            this.close.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.close.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.close.Location = new System.Drawing.Point(611, 6);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(17, 17);
            this.close.TabIndex = 5;
            this.close.UseVisualStyleBackColor = false;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 7.14F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 88);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "Пароль на сайте RuElSoft";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 7.14F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "Логин на сайте RuElSoft";
            // 
            // ruelsoft_login_action
            // 
            this.ruelsoft_login_action.BackgroundImage = global::RuelSkype.code.Properties.Resources.button;
            this.ruelsoft_login_action.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ruelsoft_login_action.FlatAppearance.BorderSize = 0;
            this.ruelsoft_login_action.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.ruelsoft_login_action.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.ruelsoft_login_action.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ruelsoft_login_action.Location = new System.Drawing.Point(12, 126);
            this.ruelsoft_login_action.Name = "ruelsoft_login_action";
            this.ruelsoft_login_action.Size = new System.Drawing.Size(114, 24);
            this.ruelsoft_login_action.TabIndex = 2;
            this.ruelsoft_login_action.Text = "Войти";
            this.ruelsoft_login_action.UseVisualStyleBackColor = true;
            this.ruelsoft_login_action.Click += new System.EventHandler(this.ruelsoft_login_action_Click);
            // 
            // ruelsoft_password
            // 
            this.ruelsoft_password.Font = new System.Drawing.Font("Tahoma", 7.1F, System.Drawing.FontStyle.Bold);
            this.ruelsoft_password.Location = new System.Drawing.Point(187, 84);
            this.ruelsoft_password.Name = "ruelsoft_password";
            this.ruelsoft_password.Size = new System.Drawing.Size(211, 19);
            this.ruelsoft_password.TabIndex = 1;
            this.ruelsoft_password.UseSystemPasswordChar = true;
            // 
            // ruelsoft_login
            // 
            this.ruelsoft_login.Font = new System.Drawing.Font("Tahoma", 7.1F, System.Drawing.FontStyle.Bold);
            this.ruelsoft_login.Location = new System.Drawing.Point(187, 47);
            this.ruelsoft_login.Name = "ruelsoft_login";
            this.ruelsoft_login.Size = new System.Drawing.Size(211, 19);
            this.ruelsoft_login.TabIndex = 0;
            // 
            // LoginForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.BackgroundImage = global::RuelSkype.code.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(637, 281);
            this.Controls.Add(this.remember_me);
            this.Controls.Add(this.linkLabel2);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.error);
            this.Controls.Add(this.close);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ruelsoft_login_action);
            this.Controls.Add(this.ruelsoft_password);
            this.Controls.Add(this.ruelsoft_login);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoginForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RuElSkypeManager";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ruelsoft_login;
        private RuElButton ruelsoft_login_action;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox ruelsoft_password;
        private RuelSkype.code.RuElButton close;
        private System.Windows.Forms.Label error;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.CheckBox remember_me;
    }
}