﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RuelSkype.code;
namespace RuelSkype.ui
{
    public partial class SearchForm : RuElForm
    {
        public SearchForm()
        {
            InitializeComponent();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void search_Click(object sender, EventArgs e)
        {
            this.UseWaitCursor = true;
            SkypeWrapper sw = SkypeWrapper.getInstance();
            if (this.search_text.Text.Length > 0)
            {
                RuElEvents.getInstance().riesSearchCompleteEvent(sw.search(this.search_text.Text));
            }
            this.UseWaitCursor = false;
            this.Close();
        }
    }
}
