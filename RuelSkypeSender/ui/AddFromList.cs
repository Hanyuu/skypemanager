﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RuelSkype.code;
using System.Threading;
namespace RuelSkype.ui
{
    public enum TList
    {
        chatList,
        categorysList,
    }
    public partial class AddFromList : RuElForm
    {
        private TList list;
        public AddFromList(TList list)
        {
            this.list = list;
            InitializeComponent();
            this.addToList();
        }


        private void addToList()
        {
            SkypeWrapper sw = SkypeWrapper.getInstance(); try
            {
                switch (this.list)
                {
                    case TList.chatList:
                        this.chats_table.DataSource = sw.getChats().Values.ToArray<SkypeChat>();
                        break;
                    case TList.categorysList:
                        this.chats_table.DataSource = sw.getCategorys().Values.ToArray<SkypeCategory>();
                        break;
                    default:
                        MessageBox.Show("shit happens");
                        break;

                }
            }
            catch (Exception e)
            {
                MessageBox.Show(String.Format("{0}\n{1}", e.StackTrace, e.Message));
                System.Console.WriteLine("{0}\n{1}", e.StackTrace, e.Message);
            }
        }
        private void search_Click(object sender, EventArgs e)
        {
            this.chats_table.Rows.Clear();
            SkypeWrapper sw = SkypeWrapper.getInstance();
            this.chats_table.DataSource = sw.filterChatsByString(this.search_string.Text);
            
        }

        private void select_all_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.chats_table.Rows)
            {
                row.Cells[this.table_select.Name].Value = !Convert.ToBoolean(row.Cells[this.table_select.Name].Value); ;
            }
        }

        private void cancle_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void add_Click(object sender, EventArgs e)
        {
            this.UseWaitCursor = true;
            List<String> names=(from row in this.chats_table.Rows.Cast<DataGridViewRow>().AsParallel()
             where row.Cells[this.table_select.Name].Value != null && Convert.ToBoolean(row.Cells[this.table_select.Name].Value)
             select (String)row.Cells[this.table_chat_title.Name].Value).ToList();

            SkypeWrapper sw = SkypeWrapper.getInstance();
            List<SkypeUser> users = null;

            switch (this.list)
            {
                case TList.chatList:
                    users = sw.getUsersFromChats(names);
                    break;
                case TList.categorysList:
                    users = sw.getUsersFromGroups(names);
                    break;
            }

            RuElEvents.getInstance().riesSearchCompleteEvent(users);
            this.UseWaitCursor = false;
            this.Close();
            /*foreach (DataGridViewRow row in this.chats_table.Rows)
            {
                if (row.Cells[this.table_select.Name].Value != null && Convert.ToBoolean(row.Cells[this.table_select.Name].Value))
                {
                    selectedValues.Add(row.Cells[this.table_chat_title.Name].Value);
                    SkypeWrapper sw = SkypeWrapper.getInstance();
                    List<SkypeUser> users = null;
                    switch (this.list)
                    {
                        case TList.chatList:
                            users = sw.getUsersFromChats(selectedValues);
                            break;
                        case TList.categorysList:
                            users = sw.getUsersFromGroups(selectedValues);
                            break;
                    }
                    RuElEvents.getInstance().riesSearchCompleteEvent(users);
                    this.Close();
                }
            }*/
        }

        private void search_string_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                switch (this.list)
                {
                    case TList.chatList:
                        this.chats_table.DataSource = SkypeWrapper.getInstance().filterChatsByString(this.search_string.Text);
                        break;
                    case TList.categorysList:
                        this.chats_table.DataSource = SkypeWrapper.getInstance().filterCategorysByString(this.search_string.Text);
                        break;
                }
            }
            catch (Exception ee)
            {
                System.Console.WriteLine("{0}\n{1}", ee.StackTrace, ee.Message);
            }
        }
    }
}
