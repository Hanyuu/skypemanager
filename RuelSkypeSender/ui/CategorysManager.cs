﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RuelSkype.code;
using System.Threading;

namespace RuelSkype.ui
{
    public partial class CategorysManager : RuElForm
    {
        public CategorysManager()
        {
            InitializeComponent();
            this.addToList();
        }

        private void addToList()
        {
            
            SkypeWrapper sw = SkypeWrapper.getInstance();
            this.UseWaitCursor = true;
            this.categorys_table.DataSource = sw.getCategorys().Values.ToArray<SkypeCategory>();
            this.UseWaitCursor = false;
        }

        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void create_category_Click(object sender, EventArgs e)
        {
            SkypeWrapper sw = SkypeWrapper.getInstance();
            if (this.category_name.Text.Length > 0)
            {
                sw.createCategory(this.category_name.Text);
            }
            this.addToList();
            this.category_name.Text = "";
        }

        private void add_category_Click(object sender, EventArgs e)
        {
            SkypeWrapper sw = SkypeWrapper.getInstance();

            List<String> categorys = (from row
                                        in this.categorys_table.Rows.Cast<DataGridViewRow>().AsParallel()
                                    where row.Cells[this.table_select.Name] != null 
                                    && Convert.ToBoolean(row.Cells[this.table_select.Name].Value)
                                      select (String)row.Cells[this.table_title.Name].Value).ToList();

            RuElEvents.getInstance().riesAddToCategoryEventEvent(categorys);
            this.Close();
        }

        private void delete_category_Click(object sender, EventArgs e)
        {
            SkypeWrapper sw = SkypeWrapper.getInstance();
            foreach (DataGridViewRow row in this.categorys_table.Rows)
            {
                if (row.Cells[this.table_select.Name] != null && Convert.ToBoolean(row.Cells[this.table_select.Name].Value))
                {
                    sw.deleteCategory(Convert.ToString(row.Cells[this.table_title.Name].Value).Trim());
                }
            }
            this.addToList();
        }

    }
}
