﻿namespace RuelSkype.ui
{
    partial class CategorysManager
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CategorysManager));
            this.label1 = new System.Windows.Forms.Label();
            this.add_category = new RuelSkype.code.RuElButton();
            this.delete_category = new RuelSkype.code.RuElButton();
            this.create_category = new RuelSkype.code.RuElButton();
            this.category_name = new System.Windows.Forms.TextBox();
            this.categorys_table = new System.Windows.Forms.DataGridView();
            this.close = new RuelSkype.code.RuElButton();
            this.table_select = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.table_title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.table_user_count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.categorys_table)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.1F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(9, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 19);
            this.label1.TabIndex = 5;
            this.label1.Text = "Добавить в категорию";
            // 
            // add_category
            // 
            this.add_category.BackgroundImage = global::RuelSkype.code.Properties.Resources.button;
            this.add_category.Cursor = System.Windows.Forms.Cursors.Hand;
            this.add_category.FlatAppearance.BorderSize = 0;
            this.add_category.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.add_category.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.add_category.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.add_category.Location = new System.Drawing.Point(225, 317);
            this.add_category.Margin = new System.Windows.Forms.Padding(2);
            this.add_category.Name = "add_category";
            this.add_category.Size = new System.Drawing.Size(86, 20);
            this.add_category.TabIndex = 4;
            this.add_category.Text = "Добавить";
            this.add_category.UseVisualStyleBackColor = true;
            this.add_category.Click += new System.EventHandler(this.add_category_Click);
            // 
            // delete_category
            // 
            this.delete_category.BackgroundImage = global::RuelSkype.code.Properties.Resources.button;
            this.delete_category.Cursor = System.Windows.Forms.Cursors.Hand;
            this.delete_category.FlatAppearance.BorderSize = 0;
            this.delete_category.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.delete_category.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.delete_category.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.delete_category.Location = new System.Drawing.Point(8, 317);
            this.delete_category.Margin = new System.Windows.Forms.Padding(2);
            this.delete_category.Name = "delete_category";
            this.delete_category.Size = new System.Drawing.Size(86, 20);
            this.delete_category.TabIndex = 3;
            this.delete_category.Text = "Удалить категорию";
            this.delete_category.UseVisualStyleBackColor = true;
            this.delete_category.Click += new System.EventHandler(this.delete_category_Click);
            // 
            // create_category
            // 
            this.create_category.Cursor = System.Windows.Forms.Cursors.Hand;
            this.create_category.FlatAppearance.BorderSize = 0;
            this.create_category.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.create_category.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.create_category.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.create_category.Image = global::RuelSkype.code.Properties.Resources.button;
            this.create_category.Location = new System.Drawing.Point(212, 30);
            this.create_category.Margin = new System.Windows.Forms.Padding(2);
            this.create_category.Name = "create_category";
            this.create_category.Size = new System.Drawing.Size(86, 20);
            this.create_category.TabIndex = 2;
            this.create_category.Text = "Создать категорию";
            this.create_category.UseVisualStyleBackColor = true;
            this.create_category.Click += new System.EventHandler(this.create_category_Click);
            // 
            // category_name
            // 
            this.category_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.category_name.Location = new System.Drawing.Point(9, 30);
            this.category_name.Margin = new System.Windows.Forms.Padding(2);
            this.category_name.Multiline = true;
            this.category_name.Name = "category_name";
            this.category_name.Size = new System.Drawing.Size(200, 20);
            this.category_name.TabIndex = 1;
            // 
            // categorys_table
            // 
            this.categorys_table.AllowUserToAddRows = false;
            this.categorys_table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.categorys_table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.table_select,
            this.table_title,
            this.table_user_count});
            this.categorys_table.Location = new System.Drawing.Point(6, 63);
            this.categorys_table.Margin = new System.Windows.Forms.Padding(2);
            this.categorys_table.Name = "categorys_table";
            this.categorys_table.RowHeadersVisible = false;
            this.categorys_table.RowTemplate.Height = 24;
            this.categorys_table.Size = new System.Drawing.Size(307, 244);
            this.categorys_table.TabIndex = 0;
            // 
            // close
            // 
            this.close.BackColor = System.Drawing.Color.Transparent;
            this.close.BackgroundImage = global::RuelSkype.code.Properties.Resources.close;
            this.close.Cursor = System.Windows.Forms.Cursors.Hand;
            this.close.FlatAppearance.BorderSize = 0;
            this.close.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.close.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.close.Location = new System.Drawing.Point(294, 6);
            this.close.Margin = new System.Windows.Forms.Padding(2);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(15, 15);
            this.close.TabIndex = 6;
            this.close.UseVisualStyleBackColor = false;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // table_select
            // 
            this.table_select.HeaderText = "";
            this.table_select.Name = "table_select";
            this.table_select.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.table_select.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.table_select.Width = 20;
            // 
            // table_title
            // 
            this.table_title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.table_title.DataPropertyName = "title";
            this.table_title.HeaderText = "Название категории";
            this.table_title.Name = "table_title";
            this.table_title.ReadOnly = true;
            // 
            // table_user_count
            // 
            this.table_user_count.DataPropertyName = "user_count";
            this.table_user_count.HeaderText = "Количество пользователей";
            this.table_user_count.Name = "table_user_count";
            this.table_user_count.ReadOnly = true;
            // 
            // CategorysManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::RuelSkype.code.Properties.Resources.background;
            this.ClientSize = new System.Drawing.Size(323, 351);
            this.Controls.Add(this.close);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.add_category);
            this.Controls.Add(this.delete_category);
            this.Controls.Add(this.create_category);
            this.Controls.Add(this.category_name);
            this.Controls.Add(this.categorys_table);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CategorysManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CategorysManager";
            ((System.ComponentModel.ISupportInitialize)(this.categorys_table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView categorys_table;
        private System.Windows.Forms.TextBox category_name;
        private RuelSkype.code.RuElButton create_category;
        private RuelSkype.code.RuElButton delete_category;
        private RuelSkype.code.RuElButton add_category;
        private System.Windows.Forms.Label label1;
        private code.RuElButton close;
        private System.Windows.Forms.DataGridViewCheckBoxColumn table_select;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_title;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_user_count;
    }
}