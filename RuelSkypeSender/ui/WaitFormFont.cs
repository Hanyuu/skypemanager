﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RuelSkype.ui
{
    public class WaitFontForm : Form
    {
        public WaitFontForm()
        {
            this.AllowTransparency = true;
            this.BackgroundImageLayout = ImageLayout.None;
            this.FormBorderStyle = FormBorderStyle.None;
            this.Opacity = 0.4;
            this.ShowInTaskbar = false;
        }
    }
}