﻿namespace RuelSkype.ui
{
    partial class AddFromList
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddFromList));
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.select_all = new RuelSkype.code.RuElButton();
            this.cancle = new RuelSkype.code.RuElButton();
            this.add = new RuelSkype.code.RuElButton();
            this.search_string = new System.Windows.Forms.TextBox();
            this.chats_table = new System.Windows.Forms.DataGridView();
            this.table_select = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.table_chat_title = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.users_count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chats_table)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(65, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Шаг 1. Добавление пользователей (из чатов)";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::RuelSkype.code.Properties.Resources.background1;
            this.panel1.Controls.Add(this.select_all);
            this.panel1.Controls.Add(this.cancle);
            this.panel1.Controls.Add(this.add);
            this.panel1.Controls.Add(this.search_string);
            this.panel1.Controls.Add(this.chats_table);
            this.panel1.Location = new System.Drawing.Point(4, 25);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(380, 338);
            this.panel1.TabIndex = 0;
            // 
            // select_all
            // 
            this.select_all.BackgroundImage = global::RuelSkype.code.Properties.Resources.button;
            this.select_all.Cursor = System.Windows.Forms.Cursors.Hand;
            this.select_all.FlatAppearance.BorderSize = 0;
            this.select_all.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.select_all.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.select_all.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.select_all.Location = new System.Drawing.Point(74, 305);
            this.select_all.Margin = new System.Windows.Forms.Padding(2);
            this.select_all.Name = "select_all";
            this.select_all.Size = new System.Drawing.Size(86, 20);
            this.select_all.TabIndex = 5;
            this.select_all.Text = "Выбрать все";
            this.select_all.UseVisualStyleBackColor = true;
            this.select_all.Click += new System.EventHandler(this.select_all_Click);
            // 
            // cancle
            // 
            this.cancle.BackgroundImage = global::RuelSkype.code.Properties.Resources.button;
            this.cancle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cancle.FlatAppearance.BorderSize = 0;
            this.cancle.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cancle.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cancle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancle.Location = new System.Drawing.Point(289, 305);
            this.cancle.Margin = new System.Windows.Forms.Padding(2);
            this.cancle.Name = "cancle";
            this.cancle.Size = new System.Drawing.Size(86, 20);
            this.cancle.TabIndex = 4;
            this.cancle.Text = "Отмена";
            this.cancle.UseVisualStyleBackColor = true;
            this.cancle.Click += new System.EventHandler(this.cancle_Click);
            // 
            // add
            // 
            this.add.BackgroundImage = global::RuelSkype.code.Properties.Resources.button;
            this.add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.add.FlatAppearance.BorderSize = 0;
            this.add.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.add.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.add.Location = new System.Drawing.Point(189, 305);
            this.add.Margin = new System.Windows.Forms.Padding(2);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(86, 20);
            this.add.TabIndex = 3;
            this.add.Text = "Ок";
            this.add.UseVisualStyleBackColor = true;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // search_string
            // 
            this.search_string.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.search_string.Location = new System.Drawing.Point(4, 17);
            this.search_string.Margin = new System.Windows.Forms.Padding(2);
            this.search_string.Multiline = true;
            this.search_string.Name = "search_string";
            this.search_string.Size = new System.Drawing.Size(371, 20);
            this.search_string.TabIndex = 1;
            this.search_string.KeyUp += new System.Windows.Forms.KeyEventHandler(this.search_string_KeyUp);
            // 
            // chats_table
            // 
            this.chats_table.AllowUserToAddRows = false;
            this.chats_table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.chats_table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.table_select,
            this.table_chat_title,
            this.users_count});
            this.chats_table.Location = new System.Drawing.Point(0, 55);
            this.chats_table.Margin = new System.Windows.Forms.Padding(2);
            this.chats_table.Name = "chats_table";
            this.chats_table.RowHeadersVisible = false;
            this.chats_table.RowTemplate.Height = 24;
            this.chats_table.Size = new System.Drawing.Size(380, 245);
            this.chats_table.TabIndex = 0;
            // 
            // table_select
            // 
            this.table_select.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.table_select.FillWeight = 30.45685F;
            this.table_select.HeaderText = "";
            this.table_select.Name = "table_select";
            this.table_select.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.table_select.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // table_chat_title
            // 
            this.table_chat_title.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.table_chat_title.DataPropertyName = "title";
            this.table_chat_title.FillWeight = 169.5431F;
            this.table_chat_title.HeaderText = "Заголовок";
            this.table_chat_title.Name = "table_chat_title";
            // 
            // users_count
            // 
            this.users_count.DataPropertyName = "user_count";
            this.users_count.HeaderText = "Кол-во человек";
            this.users_count.Name = "users_count";
            // 
            // AddFromList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::RuelSkype.code.Properties.Resources.main_form_background;
            this.ClientSize = new System.Drawing.Size(394, 367);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AddFromList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddFromChatList";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chats_table)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView chats_table;
        private System.Windows.Forms.TextBox search_string;
        private RuelSkype.code.RuElButton select_all;
        private RuelSkype.code.RuElButton cancle;
        private RuelSkype.code.RuElButton add;
        private System.Windows.Forms.DataGridViewCheckBoxColumn table_select;
        private System.Windows.Forms.DataGridViewTextBoxColumn table_chat_title;
        private System.Windows.Forms.DataGridViewTextBoxColumn users_count;
    }
}