﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RuelSkype.code;
using System.Drawing.Drawing2D;
namespace RuelSkype.ui
{
    public class RuElForm : Form
    {
        public RuElForm()
        {
            this.AllowTransparency = true;
            this.ShowInTaskbar = false;
            this.VisibleChanged += delegate(Object sender,EventArgs e)
            {
                if (this.Visible)
                {
                    Point origin = new Point(SystemInformation.Border3DSize.Width, SystemInformation.CaptionHeight);
                    GraphicsPath path = CreateRoundedRectangle(0, 0, this.Width, this.Height, 20);
                    this.Region = new Region(path);
                }
            };
        }

        public GraphicsPath CreateRoundedRectangle(float x, float y, float width, float height, float d)
        {
            var path = new GraphicsPath();
            float r = d / 2f;
            path.AddLine(x + r, y, x + width - r, y);
            path.AddArc(x + width - d, y, d, d, 270, 90);
            path.AddLine(x + width, y + r, x + width, y + height - r);
            path.AddArc(x + width - d, y + height - d, d, d, 0, 90);
            path.AddLine(x + width - r, y + height, x + r, y + height);
            path.AddArc(x, y + height - d, d, d, 90, 90);
            path.AddLine(x, y + height - r, x, y + r);
            path.AddArc(x, y, d, d, 180, 90);
            return path;
        }
        const int WM_NCHITTEST = 0x0084;
        const int HTCAPTION = 2;
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_NCHITTEST)
            {
                m.Result = (IntPtr)HTCAPTION;
                return;
            }
            base.WndProc(ref m);
        }
    }
}
