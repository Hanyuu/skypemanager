﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RuelSkype.code;
using System.Drawing.Drawing2D;
using Microsoft.Win32;
using System.Threading;
using System.Diagnostics;
namespace RuelSkype.ui
{
    public partial class LoginForm : RuElForm
    {
        private static LoginForm instance;

        public static LoginForm getInstance()
        {
            if (instance == null)
            {
                instance = new LoginForm();
            }
            return instance;
        }
        public LoginForm()
        {
            if (new StackFrame(1).GetMethod().Name != "Start")
            {
                throw new Exception("FUCK");
            }

            try
            {

                
                String login = (String)Registry.GetValue("HKEY_CURRENT_USER\\Software\\RuElSoft\\RuElSkypeManager", "login", "");
                String password = (String)Registry.GetValue("HKEY_CURRENT_USER\\Software\\RuElSoft\\RuElSkypeManager", "password", "");

                InitializeComponent();
                instance = this;
                this.ShowInTaskbar = true;
                if (login.Length > 0 && password.Length > 0)
                {
                    this.remember_me.Checked = true;
                }
                this.ruelsoft_login.Text = login;
                this.ruelsoft_password.Text = password;
            }
            catch (Exception e)
            {
                System.Console.WriteLine("{0}\n{1}", e.StackTrace, e.Message);
            }

        }

        private void setErrorText(String text)
        {
            try
            {
                this.error.Text = text;
            }
            catch (Exception e)
            {
                Invoke((MethodInvoker)(() => this.error.Text = text));
            }

        }
        private void ruelsoft_login_action_Click(object sender, EventArgs e)
        {
            try
            {
                    Network net = new Network();
                    int result = net.ruel_login(this.ruelsoft_login.Text, this.ruelsoft_password.Text);
                    switch (result)
                    {
                        case 404:
                            this.setErrorText( "Продукт не доступен в статусе \"пассивный\".");
                            break;
                        case 403:
                            this.setErrorText("Не правельный логин или пароль");
                            break;
                        case 401:
                            this.setErrorText("Срок действия продукта истёк");
                            break;
                        case 402:
                            this.setErrorText("Продукт вами не куплен");
                            break;
                        case 500:
                            this.setErrorText("Ошибка работы с сетью, поробуйте снова.\nЕсли ошибка повторится - обратитесь в службу тех.поддержки.");
                            break;
                        default:
                            if (this.remember_me.Checked)
                            {
                                Registry.SetValue("HKEY_CURRENT_USER\\Software\\RuElSoft\\RuElSkypeManager", "login", this.ruelsoft_login.Text);
                                Registry.SetValue("HKEY_CURRENT_USER\\Software\\RuElSoft\\RuElSkypeManager", "password", this.ruelsoft_password.Text);
                            }
                            else
                            {
                                Registry.SetValue("HKEY_CURRENT_USER\\Software\\RuElSoft\\RuElSkypeManager", "login", "");
                                Registry.SetValue("HKEY_CURRENT_USER\\Software\\RuElSoft\\RuElSkypeManager", "password", "");
                            }
                            this.Visible = false;
                            break;

                    }
                
                if (this.ruelsoft_login.Text.Length == 0 || this.ruelsoft_password.Text.Length == 0)
                {
                    this.error.Text = "Введите ваш логин и пароль";
                }
                
            }
            catch (Exception ee)
            {
                System.Console.WriteLine("{0}\n{1}", ee.StackTrace, ee.Message);
                /*if (main.isDebug)
                {

                    MessageBox.Show(this, String.Format("{0}\n{1}", ee.StackTrace, ee.Message));
                }*/
            }
        }

        private void close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://ruelsoft.com/users/signup/");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://ruelsoft.com/users/password/reset/");
            
        }

    }
}
