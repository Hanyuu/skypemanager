﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RuelSkype.code;
namespace RuelSkype.ui
{
    public partial class Error : RuElForm
    {
        public Error(String error)
        {
            InitializeComponent();
            this.error_text.Text = error;
        }

        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
