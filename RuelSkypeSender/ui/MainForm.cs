﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RuelSkype.code;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Threading;

namespace RuelSkype.ui
{
    public partial class MainForm : RuElForm
    {
        private OpenFileDialog ofd = new OpenFileDialog();
        private SaveFileDialog sfd = new SaveFileDialog();
        private Form errorFont = new WaitFontForm();
        private Form activeChidForm;   
        private Label waitIcon = new Label();
            
        
                
        public MainForm(String fullName)
        {
            try
            {


                InitializeComponent();

                waitIcon.Image = global::RuelSkype.code.Properties.Resources.wait;
                waitIcon.Width = global::RuelSkype.code.Properties.Resources.wait.Width;
                waitIcon.Height = global::RuelSkype.code.Properties.Resources.wait.Height;

                errorFont.Width = this.Width;
                errorFont.Height = this.Height;
                
                errorFont.MouseDown += this.returnFocusToForm;

                this.ofd.Filter = "CSV файлы (*.csv)|";
                this.sfd.Filter = "CSV файлы (*.csv)|";
                this.Text = this.Text + "(" + fullName + ")";
                this.ShowInTaskbar = true;
                

                this.name.Text = "Здравствуйте, "+fullName;
                this.version.Text = Network.version;

                this.countries.Items.AddRange(Utils.getCountries().ToArray());

                this.languages.Items.AddRange(Utils.getLangs().ToArray());
                /*foreach (Language language in Utils.getLangs().AsParallel())
                {
                    this.languages.Items.Add(language);
                }*/

                RuElEvents.getInstance().SearchCompleteEvent += new RuElEvents.SearchCompleteEventHandler(this.searchComplete);

                RuElEvents.getInstance().ErrorEvent += new RuElEvents.ErrorEventHandler(delegate(String  msg)
                {
                    Error error= new Error(msg);
                    Invoke((MethodInvoker)(() => this.showChildForm(error)));
                });

                RuElEvents.getInstance().AddToCategoryEvent += new RuElEvents.AddToCategoryEventHandler(delegate(List<String> categorys)
                {
                    SkypeWrapper.getInstance().massAddToCategorys(categorys, this.getSelectedLogins());
                });

                RuElEvents.getInstance().LogRecordEvent += new RuElEvents.LogRecordEventHandler(delegate(String msg)
                {
                    Invoke((MethodInvoker)(() => this.log.Items.Add(msg)));
                    Invoke((MethodInvoker)(() => this.log.SelectedIndex = this.log.Items.Count - 1));
                });

                RuElEvents.getInstance().HideFontEvent += new RuElEvents.HideFontEventHandler(delegate()
                {
                    Invoke((MethodInvoker)(() => this.errorFont.Visible = false));
                });


                RuElEvents.getInstance().ShowWaitFormEvent += new RuElEvents.ShowWaitFormEventHandler(delegate()
                {
                    Invoke((MethodInvoker)(() => this.showWaitWindow()));
                });


                RuElEvents.getInstance().HideWaitFormEvent += new RuElEvents.HideWaitFormEventHandler(delegate()
                {
                    Invoke((MethodInvoker)(() => this.hideWaitWindow()));
                });

                
                this.FormClosed += new FormClosedEventHandler(delegate(object sender, FormClosedEventArgs e)
                {
                    Application.Exit();
                });
            }
            catch (Exception e)
            {
                System.Console.WriteLine("{0}\n{1}", e.StackTrace, e.Message);
                MessageBox.Show(String.Format("{0}\n{1}", e.StackTrace, e.Message));
            }

        }

        public void showChildForm(Form form)
        {
            errorFont.Visible = true;
            errorFont.Bounds = this.Bounds;
            errorFont.BringToFront();
            errorFont.Owner = this;
            form.Visible = true;
            form.BringToFront();
            form.Owner = this;
            this.activeChidForm = form;
            form.FormClosed += delegate(Object sender, FormClosedEventArgs e)
            {
                this.activeChidForm = null;
                errorFont.Visible = false;
                this.WindowState = FormWindowState.Normal;
                this.BringToFront();
            };
        }

        public void returnFocusToForm(Object sender, MouseEventArgs e)
        {
            if (this.activeChidForm != null)
            {
                this.activeChidForm.BringToFront();
            }
        }
        
        private void searchComplete(List<SkypeUser> users)
        {
            
            if (!this.dont_clear.Checked)
            {
                //this.users_table.DataSource = typeof(Deposit);
            }

            this.users_table.DataSource = users;

            /*users.ForEach(delegate(SkypeUser user) 
            {
                view.Add(user.asArray());
            });*/


            this.users_count.Text = String.Format("Всего {0} / Выбрано {1}",this.users_table.RowCount-1,this.getSelectedLogins().Count);
            Utils.addLogRecord("Добавлено {0} человек", new Object[]{users.Count});
        }

        private void close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void showWaitWindow()
        {

            this.errorFont.Controls.Add(waitIcon);
            this.showChildForm(this.errorFont);
            waitIcon.Visible = true;
            waitIcon.SetBounds((this.errorFont.Width / 2), (this.errorFont.Height / 2) - waitIcon.Height, waitIcon.Width, waitIcon.Height);
            waitIcon.BringToFront();
        }

        private void hideWaitWindow()
        {
            waitIcon.Visible = false;
            this.errorFont.Visible = false;
            this.errorFont.Controls.Clear();
        }

        private void load_users_Click(object sender, EventArgs e)
        {
            if (this.from_search.Checked)
            {
                SearchForm sf = new SearchForm();
                this.showChildForm(sf);
                /*new Thread(new ThreadStart(delegate()
                {
                    SearchForm sf = new SearchForm();
                    Invoke((MethodInvoker)(() => this.showChildForm(sf)));
                })).Start();*/
            }
            else if (this.from_contacts.Checked)
            {
                this.showWaitWindow();
                new Thread(new ThreadStart(delegate()
                {
                    List<SkypeUser> result = SkypeWrapper.getInstance().getMyContacts().Values.ToList();
                    Invoke((MethodInvoker)(() => this.hideWaitWindow()));
                    Invoke((MethodInvoker)(() => this.searchComplete(result)));
                })).Start();
            }
            else if (this.from_chats.Checked)
            {
                /*AddFromList afc = new AddFromList(TList.chatList);
                this.showChildForm(afc);*/
                this.showWaitWindow();
                new Thread(new ThreadStart(delegate()
                {
                    AddFromList afc = new AddFromList(TList.chatList);
                    Invoke((MethodInvoker)(() => this.hideWaitWindow()));
                    Invoke((MethodInvoker)(() => this.showChildForm(afc)));
                })).Start();
            }
            else if (this.from_categorys.Checked)
            {
                this.showWaitWindow();
                new Thread(new ThreadStart(delegate()
                {
                    AddFromList afc = new AddFromList(TList.categorysList);
                    Invoke((MethodInvoker)(() => this.hideWaitWindow()));
                    Invoke((MethodInvoker)(() => this.showChildForm(afc)));
                })).Start();
            }

            else if (this.from_csv.Checked)
            {

                if (this.ofd.ShowDialog() == DialogResult.OK)
                {
                    this.showWaitWindow();
                    new Thread(new ThreadStart(delegate()
                    {
                        List<SkypeUser> result = Utils.parseCSVFile(this.ofd.FileName);
                        Invoke((MethodInvoker)(() => this.hideWaitWindow()));
                        Invoke((MethodInvoker)(() => this.searchComplete(result)));
                    })).Start();

                }

            }
        }


        private void append_filters_Click(object sender, EventArgs e)
        {

            if (this.all_users.Checked || this.unselect_all.Checked)
            {
                foreach (DataGridViewRow row in this.users_table.Rows)
                {
                    row.Cells[this.table_select.Name].Value = this.all_users.Checked;
                }
                this.users_count.Text = String.Format("Всего {0} / Выбрано {1}", this.users_table.RowCount, this.getSelectedLogins().Count);
            }
            else if (this.filter.Checked)
            {

                List<SkypeUser> users = (List<SkypeUser>)this.users_table.DataSource;
                FilterParameters filterParameters = new FilterParameters(users);
                //Check gender

                if (this.gender_male.Checked)
                {
                    filterParameters.gender = 1;
                }
                else if (this.gender_female.Checked)
                {
                    filterParameters.gender = 2;
                }
                //Check gender
                if (this.is_friend.Checked)
                {
                    filterParameters.status = 1;
                }
                else if (this.not_friend.Checked)
                {
                    filterParameters.status = 2;
                }

                //Is online
                filterParameters.isOnline = this.is_online.Checked;

                //Is Birthday
                filterParameters.isBirthday = this.is_birthday.Checked;

                //By age
                filterParameters.byAge = this.by_age.Checked;
                if (filterParameters.byAge)
                {
                    try
                    {
                        //Start age
                        filterParameters.ageStart = Convert.ToInt32(this.age_start.Text);

                        //End Age
                        filterParameters.ageEnd = Convert.ToInt32(this.age_end.Text);
                    }
                    catch (Exception fe)
                    {
                        this.by_age.Checked = false;
                        filterParameters.byAge = this.by_age.Checked;
                        this.age_start.Text = "";
                        this.age_end.Text = "";
                    }
                }

                //By contacts count
                filterParameters.byContactsCount = this.by_contacts_count.Checked;
                if (filterParameters.byContactsCount)
                {
                    try
                    {
                        //Start count
                        filterParameters.contactsCountStart = Convert.ToInt32(this.contacts_count_start.Text);

                        //End count
                        filterParameters.contactsCountEnd = Convert.ToInt32(this.contacts_count_end.Text);
                    }
                    catch (Exception fe)
                    {
                        this.by_contacts_count.Checked = false;
                        filterParameters.byAge = this.by_age.Checked;
                        this.contacts_count_start.Text = "";
                        this.contacts_count_end.Text = "";
                    }
                }

                filterParameters.byCountry = this.by_country.Checked;

                if (filterParameters.byCountry)
                {
                    filterParameters.country = (Country)this.countries.SelectedItem;
                }

                filterParameters.byLanguage = this.by_lang.Checked;

                if (filterParameters.byLanguage)
                {
                    filterParameters.language = (Language)this.languages.SelectedItem;
                }

                this.users_table.DataSource = filterParameters.appendFilterParameters();
                this.users_count.Text = String.Format("Всего {0} / Выбрано {1}", this.users_table.RowCount - 1, this.getSelectedLogins().Count);
                
            }
        }

        public List<String> getSelectedLogins()
        {
            return (from row
                    in this.users_table.Rows.Cast<DataGridViewRow>().AsParallel()
                    where row.Cells[this.table_select.Name] != null && Convert.ToBoolean(row.Cells[this.table_select.Name].Value)
                    select (String)row.Cells[this.table_login.Name].Value).ToList();
        }

        private void append_action_Click(object sender, EventArgs e)
        {
            
            List<String> logins = this.getSelectedLogins();
            if (logins.Count == 0)
            {
                RuElEvents.getInstance().riesErrorEvent("Вы должны выбрать пользавателей!");
                return;
            }
            SkypeWrapper sw = SkypeWrapper.getInstance();
            if (this.send_message.Checked)
            {

                sw.massSendMessage(logins, this.message.Text,TMessage.skypeMessage);
            }
            else if (this.add_to_contacts.Checked)
            {
                sw.massAddToContacts(logins,this.message.Text);
            }
            else if (this.send_sms.Checked)
            {
                sw.massSendMessage(logins, this.message.Text, TMessage.SMSMessage);
            }
            else if (this.add_to_category.Checked)
            {
                CategorysManager cm = new CategorysManager();
                this.showChildForm(cm);
            }
            else if (this.save_to_csv.Checked)
            {
                if (this.sfd.ShowDialog() == DialogResult.OK)
                {
                    Utils.saveToCSV(this.sfd.FileName, logins);
                }
            }
            else if (this.delete_contacts.Checked)
            {
                sw.deleteContactsFromSkype(logins);
            }
        }

        
        private void users_table_CellMouseClick_1(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }

        private void users_table_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            /*DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)this.users_table[0, e.RowIndex];
            System.Console.WriteLine("{0} {1}",cell,cell.Value);*/
        }

        private void users_table_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            /*DataGridViewCell cell = this.users_table[0, e.RowIndex];
            System.Console.WriteLine(cell.Value);*/
        }

        private void close_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void users_table_MouseClick(object sender, MouseEventArgs e)
        {
            this.users_count.Text = String.Format("Всего {0} / Выбрано {1}", this.users_table.RowCount, this.getSelectedLogins().Count);
        }

        private void fast_search_KeyUp(object sender, KeyEventArgs e)
        {
            SkypeWrapper sw=SkypeWrapper.getInstance();

            String search_patterns=this.fast_search.Text;
            /*if (this.users_table.Rows.Count > 0)
            {*/
                this.users_table.DataSource = (from row
                          in this.users_table.Rows.Cast<DataGridViewRow>().AsParallel()
                                               where Convert.ToString(row.Cells[this.table_login.Name].Value).Contains(search_patterns)
                                               || Convert.ToString(row.Cells[this.table_full_name.Name]).Contains(search_patterns)
                                               select sw.getUserByLogin(Convert.ToString(row.Cells[this.table_login.Name].Value))).ToList();
            //}
        }

        private void fast_search_Click(object sender, EventArgs e)
        {
            this.fast_search.Text = "";
            this.fast_search.ForeColor = System.Drawing.SystemColors.WindowText;
        }

        private void fast_search_Leave(object sender, EventArgs e)
        {
            this.fast_search.Text = "Начните вводить ключевое слово...";
            this.fast_search.ForeColor = System.Drawing.SystemColors.InactiveBorder;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            Utils.checkUpdate();
        }

        private void set_status_Click(object sender, EventArgs e)
        {
            SkypeWrapper sw = SkypeWrapper.getInstance();
            sw.setStatus(message.Text);
        }
    }
}
