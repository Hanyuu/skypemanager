﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing;
namespace RuelSkype.code
{
    public class RuElButton : Button
    {
        public RuElButton()
        {
            //this.Region = new Region(CreateRoundedRectangle(this.Bounds.X, this.Bounds.Y, this.Bounds.Width, this.Bounds.Height,1));
            this.MouseHover += new EventHandler(delegate(object sender, EventArgs e )
            {

                switch (this.Name)
                {
                    case "close":
                        this.Image = global::RuelSkype.code.Properties.Resources.close_hover;
                        break;
                    case "minimize":
                        this.Image = global::RuelSkype.code.Properties.Resources.minimize_hover;
                        break;
                    default:
                        this.Image = global::RuelSkype.code.Properties.Resources.button_hover;
                        break;
                }
            });

            this.MouseDown += new MouseEventHandler(delegate(object sender, MouseEventArgs e)
            {

                switch (this.Name)
                {
                    case "close":
                        this.Image = global::RuelSkype.code.Properties.Resources.close_pressed;
                        break;
                    case "minimize":
                        this.Image = global::RuelSkype.code.Properties.Resources.minimize_pressed;
                        break;
                    default:
                        this.Image = global::RuelSkype.code.Properties.Resources.button_pressed;
                        break;
                }
            });

            this.MouseLeave += new EventHandler(delegate(object sender, EventArgs e)
            {

                switch (this.Name)
                {
                    case "close":
                        this.Image = global::RuelSkype.code.Properties.Resources.close;
                        break;
                    case "minimize":
                        this.Image = global::RuelSkype.code.Properties.Resources.minimize;
                        break;
                    default:
                        this.Image = global::RuelSkype.code.Properties.Resources.button;
                        break;
                }
            });
        }

        public GraphicsPath CreateRoundedRectangle(float x, float y, float width, float height, float d)
        {
            var path = new GraphicsPath();
            float r = d / 2f;
            path.AddLine(x + r, y, x + width - r, y);
            path.AddArc(x + width - d, y, d, d, 270, 90);
            path.AddLine(x + width, y + r, x + width, y + height - r);
            path.AddArc(x + width - d, y + height - d, d, d, 0, 90);
            path.AddLine(x + width - r, y + height, x + r, y + height);
            path.AddArc(x, y + height - d, d, d, 90, 90);
            path.AddLine(x, y + height - r, x, y + r);
            path.AddArc(x, y, d, d, 180, 90);
            return path;
        }
    }
}
