﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using RuelSkype.ui;

namespace RuelSkype.code
{
    static class main
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {

                Application.Run(new MainForm("TEST TEST"));

            }
            catch (Exception e)
            {
                MessageBox.Show(String.Format("{0}\n{1}", e.StackTrace, e.Message));
            }
        }
    }
}
