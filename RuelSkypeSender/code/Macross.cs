﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RuelSkype.code
{
    class Macross
    {
        public static String proseedMacross(String value,SkypeUser user)
        {
            if (user==null || user.user == null || value == null)
            {
                return value;
            }
            else
            {
                return value.Replace("%name%", user.user.FullName)
                    .Replace("%login%", user.getHandle());
            }
        }
    }
}
