﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Globalization;
using System.IO;
using SKYPE4COMLib;
using System.Threading.Tasks;
using System.Threading;
using Ionic.Zip;
using System.Windows.Forms;

namespace RuelSkype.code
{
    public static class Utils
    {
        public static bool isDebug = false;
        public static void saveStackTrase(Exception e)
        {
            
        }
        public static void checkUpdate()
        {
            try
            {
                new Thread(new ThreadStart(delegate()
                {
                    try
                    {
                        Network net = new Network();
                        net.update();
                        Utils.addLogRecord("Проверка наличия обновлений.");
                        
                    }
                    catch (Exception e)
                    {
                        Utils.addLogRecord("Не удалось проверить обновления, обратитесь в службу тех.поддержки. {0}", new Object[] { e.Message });
                    }
                })).Start();
            }
            catch (Exception e)
            {
                Utils.addLogRecord("Не удалось проверить обновления, обратитесь в службу тех.поддержки. {0}", new Object[] { e.Message });
            }
        }
        public static void update(byte[] zipBytes)
        {
            try
            {
                String path = System.IO.Path.GetTempPath() + Path.GetRandomFileName() + ".zip";
                String distDir = System.IO.Path.GetTempPath() + Path.GetRandomFileName();
                File.WriteAllBytes(path, zipBytes);

                ZipFile zip = ZipFile.Read(path);
                Directory.CreateDirectory(distDir);
                zip.ExtractAll(distDir);
                Directory.EnumerateFiles(distDir).ToList().ForEach(delegate(String file)
                {
                    String original = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + Path.GetFileName(file);
                    if (File.Exists(original))
                    {
                        File.Replace(file, original, original + ".back");
                    }
                    else
                    {
                        File.Copy(file, original);
                    }
                });
                Utils.addLogRecord("Обновление завершено, для применения обновлений перезапустите программу.");
            }
            catch (Exception e)
            {
                Utils.addLogRecord("Не удалось провести обновления, обратитесь в службу тех.поддержки. {0}", new Object[] { e.Message });
            }
        }
        public static void addLogRecord(String record,Object[] args=null)
        {
            RuElEvents.getInstance().riesLogRecordEvent((args == null ? record : String.Format(record, args)));
        }

        public static void saveToCSV(String path,List<String> logins)
        {
            if (!path.EndsWith(".csv"))
            {
                path += ".csv";
            }
            try
            {
                File.WriteAllLines(path, (String[])logins.ToArray());
            }
            catch (Exception e)
            {
                RuElEvents.getInstance().riesErrorEvent(String.Format("Ошибка записи в файл.\n{0}",e.Message));
            }
            Utils.addLogRecord("Сохранено {0} записей", new Object[]{logins.Count});
        }
        public static List<SkypeUser> parseCSVFile(String file)
        {
            List<SkypeUser> result = new List<SkypeUser>();
            try
            {

                Utils.addLogRecord("Начало обработки файла {0}", new Object[] { Path.GetFileName(file) });
                String[] lines = File.ReadAllLines(file);
                Utils.addLogRecord("Всего записей в файле {0}", new Object[] { lines.Length });
                SkypeWrapper sw = SkypeWrapper.getInstance();
                    Parallel.ForEach(lines, login =>
                    {
                        login = login.Trim();
                        if (login.Length > 0)
                        {
                            SkypeUser su = sw.getUserByLogin(login, true);
                            if (su == null)
                            {
                                su = new SkypeUser(login);
                                sw.addToUserCache(su);
                            }
                            result.Add(su);
                        }
                    });
                Utils.addLogRecord("Обработки файла {0} завершена", new Object[] { Path.GetFileName(file) });
            }
            catch (Exception e)
            {
                Utils.addLogRecord("Ошибка при обработки файла {0}. Файл не обработан.", new Object[] { file });
                RuElEvents.getInstance().riesErrorEvent(String.Format("Ошибка чтения из файла.\n{0}", e.Message));
            }
            RuElEvents.getInstance().riesHideFontEventEvent();
            return result;
        }

        public static IEnumerable<Language> getLangs()
        {
            return from ri in
                       from ci in CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                       select new CultureInfo(ci.ToString())
                   group ri by ri.NativeName into g
                   select new Language
                   {
                       languageId = g.Key,
                       title = g.First().DisplayName
                   } ;
        }

        public static IEnumerable<Country> getCountries()
        {
            return from ri in
                       from ci in CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                       select new RegionInfo(ci.LCID)
                   group ri by ri.TwoLetterISORegionName into g
                   select new Country
                   {
                       CountryId = g.Key,
                       Title = g.First().DisplayName
                   };
        }
        
    }

    public class Language
    {
        public string languageId { get; set; }

        public string title {get;set;}

        public override string ToString()
        {
            return this.title;
        }

    }
    public class Country
    {
        public string CountryId { get; set; }
        public string Title { get; set; }

        public override string ToString()
        {
            return this.Title;
        }
    }


}
