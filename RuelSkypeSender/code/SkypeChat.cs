﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using SKYPE4COMLib;

namespace RuelSkype.code
{
    public class SkypeChat
    {
        public IChat chat;

        public String title
        {
            get
            {
                return this.ToString();
            }
        }

        public int user_count
        {
            get
            {
                return this.chat.Members.Count;
            }
        }
        private Dictionary<String, SkypeUser> usersCache = new Dictionary<String, SkypeUser>();
        public SkypeChat(IChat chat)
        {
            this.chat = chat;
        }

        
        public override string ToString()
        {
            return this.chat.Topic+"("+this.chat.FriendlyName+")";
        }
        public override int GetHashCode()
        {
            return 0;
        }
        public override bool Equals(object obj)
        {
            return this.chat.Topic == ((SkypeChat)obj).chat.Topic && this.chat.FriendlyName == ((SkypeChat)obj).chat.FriendlyName;
        }

        public Dictionary<String, SkypeUser> getUsers(bool update = false)
        {
            if (this.usersCache.Count == 0 || update)
            {

                this.usersCache = (from user in this.chat.Members.Cast<User>() select new SkypeUser(user))
                .ToDictionary(x => x.getHandle(), x => x);
            }
            
            return this.usersCache;
        }
    }
}
