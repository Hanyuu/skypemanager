﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using SKYPE4COMLib;
namespace RuelSkype.code
{
    public class SkypeCategory
    {
        private Group _category;

        public String title
        {
            get
            {
                return this.ToString();
            }
        }

        public int user_count
        {
            get
            {
                return this._category.Users.Count;
            }
        }

        private Dictionary<String, SkypeUser> cache = new Dictionary<String, SkypeUser>();

        public SkypeCategory(Group group)
        {
            this._category = group;
        }

        public Dictionary<String, SkypeUser> getUsers(bool update=false)
        {
            if (this.cache.Count == 0 || update)
            {
                this.cache = (from user in this._category.Users.Cast<User>().AsParallel()
                              select new SkypeUser(user)).ToDictionary(x => x.getHandle(), x => x);
            }
            return this.cache;
            

        }

        public override int GetHashCode()
        {
            return 0;
        }
        public override bool Equals(object obj)
        {
            return this._category.DisplayName == ((SkypeCategory)obj)._category.DisplayName; ;
        }

        public override String ToString()
        {
            return this._category.DisplayName;
        }
    }
}
