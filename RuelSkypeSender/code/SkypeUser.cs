﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SKYPE4COMLib;
namespace RuelSkype.code
{
    public class SkypeUser
    {
        public User user=null;

        private bool _selected=false;

        public bool selected
        {
            get {return this._selected;}
            set { this._selected = value; }
        }


        private String _login;
        public String login
        {
            get { return this.getHandle(); }
        }

        public String full_name
        {
            get { return this.user == null ? "" : this.user.FullName; }

        }

        public void delete()
        {
            this.user.BuddyStatus = TBuddyStatus.budDeletedFriend;
        }
        public String is_friend
        {
            get { return this.user == null ? "Не известно" : this.user.BuddyStatus==TBuddyStatus.budFriend?"Да":"Нет"; }
        }

        public String is_online
        {
            get { return this.user == null ? "Не известно" : this.user.OnlineStatus != TOnlineStatus.olsOffline ? "В сети" : "Не в сети"; }
        }

        public int contacts_count
        {
            get { return this.user == null ? 0 : this.user.NumberOfAuthBuddies; }
        }

        public String gender
        {
            get
            {
                if (this.user == null)
                {
                    return "Не известно";
                }
                else
                {
                    String sex = "Не указан";
                    switch (this.user.Sex)
                    {
                        case TUserSex.usexFemale:
                            sex = "Женский";
                            break;
                        case TUserSex.usexMale:
                            sex = "Мужской";
                            break;
                    }
                    return sex;
                }
            }
        }

        public String language
        {
            get
            {
                 return this.user == null ? "Не известно" : this.user.Language;
            }
        }

        public String country
        {
            get
            {
                return this.user == null ? "Не известно" : this.user.Country;
            }
        }

        public String province
        {
            get
            {
                return this.user == null ? "Не известно" : this.user.Province;
            }
        }

        public String city
        {
            get
            {
                return this.user == null ? "Не известно" : this.user.City;
            }
        }

        public String last_visit
        {
            get
            {
                return this.user == null ? "Не известно" : this.user.LastOnline.ToString();
            }
        }

        public String birthday
        {
            get
            {
                return this.user == null ? "Не известно" : this.user.Birthday.ToString();
            }
        }

        public String age
        {
            get
            {
                return this.user == null ? "Не известно" : Convert.ToString(DateTime.Now.Year - this.user.Birthday.Year);
            }
        }

        public String home_page
        {
            get
            {
                return this.user == null ? "Не известно" : this.user.Homepage;
            }
        }

        public String status_text
        {
            get
            {
                return this.user == null ? "Не известно" : this.user.MoodText;
            }
        }

        public String phone_office
        {
            get
            {
                return this.user == null ? "Не известно" : this.user.PhoneOffice;
            }
        }

        public String phone_home
        {
            get
            {
                return this.user == null ? "Не известно" : this.user.PhoneHome;
            }
        }

        public String phone_mobile
        {
            get
            {
                return this.user == null ? "Не известно" : this.user.PhoneMobile;
            }
        }

        public SkypeUser(User user)
        {
            this.user = user;
        }

        public SkypeUser(String login)
        {
            this._login = login;
        }

        public override String ToString()
        {
            return this.getHandle()/*+" ("+this.user.FullName+")"*/;
        }

        public void addToContacts(String invationText)
        {
            SkypeWrapper.getInstance().addToSkype(this.getHandle(), invationText);
        }

        public String getHandle()
        {
            return this.user != null && this.user.Handle != null ? this.user.Handle : this._login;
        }

        
        public String[] asArray()
        {
            if (this.user == null || this.user.Handle==null)
            {
                return new String[] {"false",this.login};
            }
            /*Логин   
            Полное имя   
            Статус друга   
            Online-статус    
            Контакты     Пол    Язык    Страна    Область    Город    Последнее посещение    
            Дата рождение    Домашняя старница   Текст статуса    
            О пользователе     Домашний телефон    Офисный телефон    Мобильный телефон*/
            String sex = "Не указан"; //Two days in week, with hand
            switch (this.user.Sex)
            {
                case TUserSex.usexFemale:
                    sex = "Женский";
                    break;
                case TUserSex.usexMale:
                    sex="Мужской";
                    break;
            }
            return new String[] { "False",
                this.login,
                this.full_name,
                this.is_friend,
                this.is_online,
                Convert.ToString(this.contacts_count),
                this.gender,
                this.language,
                this.country,
                this.province,
                this.city,
                this.last_visit,
                this.birthday,
                this.age,
                this.home_page,
                this.status_text,
                "",
                this.phone_home,
                this.phone_office,
                this.phone_mobile};
        }
    }
}
