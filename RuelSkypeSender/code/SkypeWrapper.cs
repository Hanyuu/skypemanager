﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using SKYPE4COMLib;
using System.Windows.Forms;
using System.Threading;
namespace RuelSkype.code
{
    public enum TMessage
    {
        skypeMessage,
        SMSMessage
    }
    public class SkypeWrapper
    {
        private Skype skype = new Skype();
        private static SkypeWrapper instance;

        private Dictionary<String, SkypeChat> chatCache = new Dictionary<string, SkypeChat>();
        private Dictionary<String, SkypeCategory> categoryCache = new Dictionary<String, SkypeCategory>();
        private Dictionary<String, SkypeUser> contactsCache = new Dictionary<String, SkypeUser>();
        private Dictionary<String, SkypeUser> usersCache = new Dictionary<String, SkypeUser>();

        public SkypeWrapper()
        {
            try
            {
            this.skype.Attach(7, true);
            this.skype.MessageStatus += new _ISkypeEvents_MessageStatusEventHandler(this.onMessageStatus);
            
                /*new Thread(new ThreadStart(delegate() 
                {
                    this.getChats();
                })).Start();

                new Thread(new ThreadStart(delegate()
                {
                    this.getMyContacts();
                })).Start();

                new Thread(new ThreadStart(delegate()
                {
                    this.getCategorys();
                })).Start();*/
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message+"\n"+e.StackTrace);
            }
        }

        public static SkypeWrapper getInstance()
        {
            if (instance == null)
            {
                instance = new SkypeWrapper();
            }
            return instance;
        }

        public void setStatus(String status)
        {
            Command cmd = new Command();
            cmd.Command = "SET PROFILE RICH_MOOD_TEXT " + status;
            this.skype.SendCommand(cmd);
        }
        public void deleteContactsFromSkype(List<String> users)
        {
            this.getUsersByLogins(users).ForEach(delegate(SkypeUser user)
            {
                user.delete();
            });
        }

        public List<SkypeUser> getUsersFromGroups(List<String> groups)
        {
            List<SkypeUser> result =new  List<SkypeUser>();
            (from category in this.categoryCache.AsParallel() where groups.Contains(category.Value.ToString()) select category.Value)
                .ToList().ForEach(delegate(SkypeCategory category)
            {
                result.AddRange(category.getUsers().Values);

                category.getUsers().Values.AsParallel().ToList().ForEach(delegate(SkypeUser user)
                {
                    try
                    {
                        this.usersCache[user.getHandle()] = user;
                    }
                    catch (Exception e) { }
                });

            });

            return result;
        }
        public List<SkypeUser> getUsersByLogins(List<String> logins)
        {
            return (from user in this.usersCache.AsParallel() 
                    where logins.Contains(user.Value.getHandle()) 
                    select user.Value)
                .ToList();
        }


        public void massAddToContacts(List<String> logins, String message)
        {
            uint count = 0;
            (from user in this.usersCache.AsParallel() where logins.Contains(user.Value.getHandle()) select user.Value)
                .ToList().ForEach(delegate(SkypeUser user)
            {
                count++;
                user.addToContacts(message);
            });
            Utils.addLogRecord("Отправлено запросов на добавление в друзья {0}", new Object[] { count });
        }

        public void massSendMessage(List<String> logins, String message, TMessage type)
        {
            int successful = 0;
            int failed = 0;
            (from user in this.usersCache.AsParallel()  where logins.Contains(user.Value.getHandle()) select user.Value)
                .ToList()
                .ForEach(delegate(SkypeUser user)
                {
                    try
                    {
                        switch (type)
                        {
                            case TMessage.skypeMessage:
                                this.sendMessage(user.getHandle(), message);
                                break;
                            case TMessage.SMSMessage:
                                this.sendSMS(user.getHandle(), message);
                                break;
                        }
                        successful++;
                    }
                    catch (Exception e)
                    {
                        failed++;
                        System.Console.WriteLine("{0}\n{1}", e.StackTrace, e.Message);
                    }
                });

            Utils.addLogRecord("Отправлено {0} сообщений.\nНе удалось отправить {1}.", new Object[] { successful, failed });
        }

        public void sendSMS(String login, String message)
        {
            SkypeUser user = this.getUserByLogin(login);
            if (user.user.PhoneMobile.Length > 0)
            {
                if (user.user != null)
                {
                    this.skype.SendSms(user.user.PhoneMobile, message);
                }
                
            }
        }
        public SkypeUser getUserByLogin(String login,bool onlyFromCache=false)
        {
            SkypeUser user = null;
            try
            {
                user = this.usersCache[login];
            }
            catch (Exception e)
            {
                try
                {
                    user = this.contactsCache[login];
                }
                catch (Exception ee)
                {
                    if (!onlyFromCache)
                    {
                        user = this.tryGetFriendByLogin(login);
                        user = user == null ? this.tryGetUserFromSearch(login) : user;
                        this.usersCache.Add(login, user);
                    }
                }
            }

            return user;
        }

        public SkypeUser tryGetUserFromSearch(String login)
        {
            UserCollection users = this.skype.SearchForUsers(login);
            if (users.Count > 0)
            {
                return new SkypeUser(users[1]);
            }
            else
            {
                return null;
            }
        }

        public SkypeUser tryGetFriendByLogin(String login)
        {
            return (from user in (this.contactsCache.Count > 0 ? this.contactsCache.AsParallel() : this.getMyContacts().AsParallel()) where user.Value.getHandle() == login select user.Value).ElementAtOrDefault(0);
        }


        public Dictionary<String, SkypeUser> getMyContacts(bool update = false)
        {
            if (this.contactsCache.Count > 0 && !update)
            {
                return this.contactsCache;
            }
            else
            {
                this.contactsCache.Clear();
                this.contactsCache = (from user in this.skype.Friends.Cast<User>().AsParallel()
                                      select new SkypeUser(user)).ToDictionary(x => x.ToString(), x => x);
                this.contactsCache
                    .AsParallel()
                    .ToList()
                    .ForEach(delegate(KeyValuePair<string,SkypeUser> val)
                {
                    try
                    {
                        this.usersCache[val.Key] = val.Value;
                    }
                    catch (Exception)
                    {
                    }
                });
                return this.contactsCache;
            }
        }

        public void createCategory(String name)
        {
            this.skype.CreateGroup(name);
            this.getCategorys(true);
        }

        public void addToCategory(String group_name, String user)
        {
            (from g in this.skype.Groups.Cast<Group>().AsParallel() where g.DisplayName == group_name select g)
                .ElementAtOrDefault(0)
                .AddUser(user);
            this.getCategorys(true);
        }

        public void massAddToCategorys(List<String> categorys,List<String> users)
        {


            (from g in this.skype.Groups.Cast<Group>().AsParallel() where categorys.Contains(g.DisplayName) select g)
                .ToList()
                .ForEach(delegate(Group g)
            {

                users.ForEach(delegate(String user)
                {

                    g.AddUser(user);
                });
            });
            this.getCategorys(true);
        }

        public void deleteCategory(String group_name)
        {
            int id = (from g in this.skype.Groups.Cast<Group>().AsParallel() where g.DisplayName == group_name select g)
                .ElementAtOrDefault(0).Id;
            this.skype.DeleteGroup(id);
            this.getCategorys(true);
        }

        /**
         * TODO: Refactor this
        **/
        public List<SkypeUser> getUsersFromChats(List<String> chatsNames)
        {
            List<SkypeUser> _result = new List<SkypeUser>();

            List<Dictionary<String,SkypeUser>> result = 
                (from chat in this.chatCache.AsParallel().AsQueryable()
                 where chatsNames.Contains(chat.Value.ToString())
                 select chat.Value.getUsers(true))
                           .ToList();
            result.ForEach(delegate(Dictionary<String, SkypeUser> users)
            {
                _result.AddRange(users.Values);
                users.Values.Cast<SkypeUser>().AsParallel().ToList().ForEach(delegate(SkypeUser user)
                {
                    
                    try
                    {

                        this.usersCache[user.getHandle()] = user;
                    }
                    catch (Exception e) { }
                });

            });
            return _result;
        }


        public Dictionary<String, SkypeChat> getChats(bool update = false)
        {

            if (this.chatCache.Count > 0 && !update)
            {
                return this.chatCache;
            }
            else
            {
                this.chatCache.Clear();
                this.chatCache = (from chat in this.skype.Chats.Cast<IChat>().AsParallel()
                                  where chat.Type == TChatType.chatTypeMultiChat
                                  group chat by chat.ActivityTimestamp into chat
                                  select new SkypeChat(chat.ElementAtOrDefault(0)))
                                  .Distinct()
                                  .ToDictionary(x => x.ToString(), x => x);
                return this.chatCache;
            }
        }

        public List<SkypeChat> filterChatsByString(String search)
        {
            return( from chat in this.chatCache.Values
                   where chat.ToString().Contains(search)
                   select chat).ToList();
        }

        public List<SkypeCategory> filterCategorysByString(String search)
        {
            return (from category in this.categoryCache.Values
                    where category.ToString().Contains(search)
                    select category).ToList();
        }

        public Dictionary<String, SkypeCategory> getCategorys(bool update = false)
        {
            if (this.categoryCache.Count > 0 && !update)
            {
                return this.categoryCache;
            }
            else
            {
                this.categoryCache = (from category in this.skype.Groups.Cast<Group>()
                                      where category.DisplayName.Length > 0
                                      select new SkypeCategory(category)).Distinct().ToDictionary(x => x.ToString(), x => x);
                return this.categoryCache;
            }
        }

        public void addToSkype(String user, String invite)
        {
            Command command = new Command();

            command.Command = "SET USER " + user + " BUDDYSTATUS 2 " + Macross.proseedMacross(invite, this.getUserByLogin(user));
            this.skype.SendCommand(command);
            Utils.addLogRecord("Отправлен запрос на добавление в друзья {0}", new Object[] { user });

        }


        public void sendMessage(String userHandle, String message)
        {
            System.Console.WriteLine("{0} {1}", userHandle, message);
            try
            {
                this.skype.SendMessage(userHandle, Macross.proseedMacross(message, new SkypeUser(this.skype.get_User(userHandle))));
                Utils.addLogRecord("Сообщение отправлено {0}", new Object[] { userHandle });
            }
            catch (Exception e)
            {
                if (Utils.isDebug)
                {
                    MessageBox.Show(String.Format("{0}\n{1}\n{2}", e.StackTrace, e.Message,userHandle));
                }
            }
            
        }

        public ChatMessageCollection getMessages(String target)
        {
            return this.skype.get_Messages(target);
        }

        public List<SkypeChat> getActiveChats()
        {
            return (from chat in this.skype.ActiveChats.Cast<Chat>().AsParallel() select new SkypeChat(chat)).ToList();
        }
        public void onMessageStatus(IChatMessage message, TChatMessageStatus status)
        {
            switch (message.Type)
            {
                case TChatMessageType.cmeAddedMembers:
                case TChatMessageType.cmeJoinedAsApplicant:
                case TChatMessageType.cmeLeft:
                case TChatMessageType.cmeKicked:
                case TChatMessageType.cmeKickBanned:
                    break;
                case TChatMessageType.cmeSaid:
                    if (status == TChatMessageStatus.cmsReceived || status == TChatMessageStatus.cmsSent)
                        //UPDATE CHAT WHERE
                        break;
                    break;


            }
            System.Console.WriteLine("{0} {1} {2} {3}",message.Chat.Name, message.Type, message.Body, status);
        }
        public List<SkypeUser> search(String keyword)
        {

            List<SkypeUser> result = (from user in this.skype.SearchForUsers(keyword).Cast<User>()
                                          .AsParallel() select new SkypeUser(user))
                                          .ToList();
            result.ForEach(delegate(SkypeUser user)
            {
                try
                {
                    this.usersCache.Add(user.getHandle(), user);
                }
                catch (Exception e) { }
            });
            return result;
        }

        public void addToUserCache(SkypeUser user)
        {
            try
            {
                this.usersCache.Add(user.getHandle(), user);
            }
            catch (Exception e)
            {
                System.Console.WriteLine("{0}\n{1}",e.StackTrace,e.Message);
            }
        }
    }
}
