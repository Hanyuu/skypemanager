﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RuelSkype.code
{
    public class RuElEvents
    {
        /*
        * Error Event declaration
        */
        public delegate void ErrorEventHandler(String args);
        public event ErrorEventHandler ErrorEvent;

        /*
        * Log Record Event declaration
        */
        public delegate void LogRecordEventHandler(String args);
        public event LogRecordEventHandler LogRecordEvent;


        /*
        * Add to category Event declaration
        */
        public delegate void AddToCategoryEventHandler(List<String> categorys);
        public event AddToCategoryEventHandler AddToCategoryEvent;

       /*
       * Hide font Event declaration
       */
        public delegate void HideFontEventHandler();
        public event HideFontEventHandler HideFontEvent;

       /*
       * Show Wait Form Event declaration
       */
        public delegate void ShowWaitFormEventHandler();
        public event ShowWaitFormEventHandler ShowWaitFormEvent;

       /*
       * Hide Wait Form Event declaration
       */
        public delegate void HideWaitFormEventHandler();
        public event HideWaitFormEventHandler HideWaitFormEvent;

       /*
       * Search Complete Event declaration
       */
        public delegate void SearchCompleteEventHandler(List<SkypeUser> users);
        public event SearchCompleteEventHandler SearchCompleteEvent;

        private static RuElEvents instance;

        public static RuElEvents getInstance()
        {
            if (instance == null)
            {
                instance = new RuElEvents();
            }
            return instance;
        }

        public void riesErrorEvent(String error)
        {
            ErrorEvent(error);
        }

        public void riesLogRecordEvent(String msg)
        {
           //LogRecordEvent(msg);
        }

        public void riesAddToCategoryEventEvent(List<String> categorys)
        {
            AddToCategoryEvent(categorys);
        }

        public void riesHideFontEventEvent()
        {
            HideFontEvent();
        }

        public void riesShowWaitForm()
        {
            ShowWaitFormEvent();
        }

        public void riesHideWaitForm()
        {
            HideWaitFormEvent();
        }

        public void riesSearchCompleteEvent(List<SkypeUser> users)
        {
            SearchCompleteEvent(users);
        }
    }

}
