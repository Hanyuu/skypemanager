﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using SKYPE4COMLib;
namespace RuelSkype.code
{
    public class FilterParameters
    {
        List<SkypeUser> users;

        public FilterParameters(List<SkypeUser> logins)
        {
            this.users = logins;
        }
        public bool isOnline { get; set; }

        public bool isBirthday { get; set; }

        public int gender { get; set; }

        public int status { get; set; }

        public bool byAge { get; set; }

        public int ageStart { get; set; }

        public int ageEnd { get; set; }

        public bool byContactsCount { get; set; }

        public int contactsCountStart { get; set; }

        public int contactsCountEnd { get; set; }

        public bool byLanguage { get; set; }

        public Language language { get; set; }

        public bool byCountry { get; set; }

        public Country country { get; set; }

        public List<SkypeUser> appendFilterParameters()
        {
            return Filter.getInstance().filter(this.users, this);
        }

    }

    public class Filter
    {
        private static Filter instance;

        public static Filter getInstance()
        {
            if (instance == null)
            {
                instance = new Filter();
            }
            return instance;
        }

        public bool isBirthday(SkypeUser user,bool check=true)
        {
            if (!check)
                return true;
            if (user.user.Birthday.Year == 1900)
                return false;
            DateTime now = DateTime.Now.Date;
            return user.user.Birthday.Date.Month == now.Month && user.user.Birthday.Day == now.Day;
        }

        public bool isOnline(SkypeUser user, bool check=true)
        {
            if (!check)
                return true;
            return user.user.OnlineStatus != TOnlineStatus.olsOffline;
        }

        public bool byStatus(SkypeUser user,int status)
        {
            switch (status)
            {
                case 0:
                    return true;
                case 1:
                    return user.user.BuddyStatus == TBuddyStatus.budFriend;
                case 2:
                    return user.user.BuddyStatus == TBuddyStatus.budPendingAuthorization;
            }
            return false;
        }

        public bool byAge(SkypeUser user, int ageStart,int ageEnd,bool check=true)
        {

            if (!check)
                return true;
            if (user.user.Birthday.Year == 1900)
                return false;
            int years = DateTime.Now.Year-user.user.Birthday.Year;
            return ageStart <= years && years <= ageEnd;
        }

        public bool byContactsCount(SkypeUser user, int contactsCountStart, int contactsCountEnd, bool check = true)
        {
            if (!check)
                return true;
            return contactsCountStart <= user.user.NumberOfAuthBuddies && user.user.NumberOfAuthBuddies <= contactsCountEnd;
        }

        public bool byCountry(SkypeUser user,Country country, bool check = true)
        {
            if (!check)
                return true;
            if (user.user.CountryCode == null || user.user.CountryCode.Length == 0)
                return false;
            return user.user.CountryCode==country.CountryId.ToLower();
        }

        public bool byLanguage(SkypeUser user, Language language, bool check = true)
        {
            if (!check)
                return true;
            if (user.user.Language == null || user.user.Language.Length == 0)
                return false;
            return user.user.LanguageCode==language.languageId;
        }


        public bool byGender(SkypeUser user, int gender)
        {
            switch (gender)
            {
                case 0:
                    return true;
                case 1:
                    return user.user.Sex == TUserSex.usexMale;
                case 2:
                    return user.user.Sex == TUserSex.usexFemale;
            }
            return false;
        }

        public List<SkypeUser> filter(List<SkypeUser> logins, FilterParameters filterParamentrs)
        {

           List<SkypeUser> filtered= logins.AsParallel().Where(user=>this.byGender(user, filterParamentrs.gender)
                   && this.byStatus(user, filterParamentrs.status)
                   && this.isBirthday(user, filterParamentrs.isBirthday)
                   && this.isOnline(user, filterParamentrs.isOnline)
                   && this.byAge(user, filterParamentrs.ageStart, filterParamentrs.ageEnd, filterParamentrs.byAge)
                   && this.byContactsCount(user,filterParamentrs.contactsCountStart,filterParamentrs.contactsCountEnd,filterParamentrs.byContactsCount)
                   && this.byCountry(user,filterParamentrs.country,filterParamentrs.byCountry)
                   && this.byLanguage(user, filterParamentrs.language, filterParamentrs.byLanguage)).Select(user => { user.selected = true; return user; }).ToList();
           filtered.AddRange(logins.Except(filtered).Where(x => x != null).Select(x => { x.selected = false;return x; }));
           return filtered;



        }
    }
}
