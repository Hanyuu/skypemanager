﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Paddings;
using RuelSkype.ui;
namespace RuelSkype.code
{

    public class Network
    {
        public static String version = "1.8";
        private String host = "updates.ruelsoft.org";
        private int port = 9568;
        private static UTF8Encoding utf8 = new UTF8Encoding();
        private StreamWriter writer;
        private StreamReader reader;
        private String blowFishKey;
        private RSACryptoServiceProvider rsa;
        private Socket socket;
        private BCEngine bcEngine = new BCEngine(new BlowfishEngine(), utf8);
        private bool useBlowFish = false;

        private void init()
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(this.host);
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            //IPAddress ipAddress = IPAddress.Parse(this.host);
            if (IPAddress.IsLoopback(ipAddress))
            {
                 Application.Exit();
            }
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, this.port);
            this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                this.socket.Connect(remoteEP);
                NetworkStream nstream = new NetworkStream(this.socket);
                this.writer = new StreamWriter(nstream);
                this.reader = new StreamReader(nstream);
                this.sendInt(300);
                while (!this.useBlowFish)
                {
                    String cmd = reader.ReadLine();
                    this.handle(cmd);
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("{0}\n{1}", e.StackTrace, e.Message);
            }
        }

        public void update()
        {
            this.init();
            this.sendInt(304);
            this.sendString(version);
            String cmd = reader.ReadLine();
            this.handle(cmd);
        }
        public int ruel_login(String login, String password)
        {
            this.init();
            this.sendInt(303);
            this.sendString(this.bcEngine.Encrypt(login, this.blowFishKey));
            this.sendString(this.bcEngine.Encrypt(password, this.blowFishKey));
            String cmd = reader.ReadLine();
            return this.handle(cmd);
        }


        private void sendInt(int cmd)
        {
            String _cmd = cmd.ToString();
            if (this.useBlowFish)
            {
                _cmd = this.bcEngine.Encrypt(_cmd, this.blowFishKey);
            }
            this.sendString(_cmd);
        }

        private void sendString(String cmd)
        {
            
            this.writer.WriteLine(cmd);
            this.writer.Flush();
        }

        private int handle(String cmd)
        {
            try
            {
                if (this.useBlowFish)
                {
                    cmd = this.bcEngine.Decrypt(cmd, this.blowFishKey);
                }
                int _cmd = Int32.Parse(cmd);
                System.Console.WriteLine(_cmd);
                switch (_cmd)
                {
                    case 500:
                    case 404:
                    case 403:
                    case 402:
                    case 401:
                        return _cmd;
                    case 100:
                        this.sendInt(301);
                        break;
                    case 101:
                        this.rsa = new RSACryptoServiceProvider(512);
                        RSAParameters rsaPars = new RSAParameters();
                        rsaPars.Modulus = Convert.FromBase64String(reader.ReadLine());
                        rsaPars.Exponent = Convert.FromBase64String(reader.ReadLine());
                        this.rsa.ImportParameters(rsaPars);
                        this.sendInt(302);
                        this.blowFishKey = Path.GetRandomFileName();
                        this.sendString(Convert.ToBase64String(this.rsa.Encrypt(utf8.GetBytes(this.blowFishKey), false)));
                        this.useBlowFish = true;
                        break;
                    case 102:
                        //NOT USED
                        break;
                    case 103:
                        String fullName=this.reader.ReadLine();
                        this.socket.Close();
                        new MainForm(fullName).Visible=true;
                        break;
                    case 104:
                        String newVersion = this.reader.ReadLine();
                        byte[] bNewVersion=Convert.FromBase64String(this.bcEngine.Decrypt(newVersion,this.blowFishKey));
                        Utils.update(bNewVersion);
                        break;
                    case 105:
                        Utils.addLogRecord("Вы используете последнию версию программы.");
                        break;

                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine("{0}\n{1}", e.StackTrace, e.Message);
                return 500;
            }
            return -1;
        }
    }

    public class BCEngine
    {
        private readonly Encoding _encoding;
        private readonly IBlockCipher _blockCipher;
        private PaddedBufferedBlockCipher _cipher;
        private IBlockCipherPadding _padding;

        public BCEngine(IBlockCipher blockCipher, Encoding encoding)
        {
            _blockCipher = blockCipher;
            _encoding = encoding;
        }

        public void SetPadding(IBlockCipherPadding padding)
        {
            if (padding != null)
                _padding = padding;
        }

        public string Encrypt(string plain, string key)
        {
            byte[] result = BouncyCastleCrypto(true, _encoding.GetBytes(plain), key);
            return Convert.ToBase64String(result);
        }

        public byte[] DecryptAsBytes(string cipher, string key)
        {
            return BouncyCastleCrypto(false, Convert.FromBase64String(cipher), key);
        }
        public string Decrypt(string cipher, string key)
        {
            return _encoding.GetString(DecryptAsBytes(cipher, key));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="forEncrypt"></param>
        /// <param name="input"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        /// <exception cref="CryptoException"></exception>


        private byte[] BouncyCastleCrypto(bool forEncrypt, byte[] input, string key)
        {
            try
            {
                _cipher = _padding == null ? new PaddedBufferedBlockCipher(_blockCipher) : new PaddedBufferedBlockCipher(_blockCipher, _padding);
                byte[] keyByte = _encoding.GetBytes(key);
                _cipher.Init(forEncrypt, new KeyParameter(keyByte));
                return _cipher.DoFinal(input);
            }
            catch (Org.BouncyCastle.Crypto.CryptoException ex)
            {
                throw ex;
            }
        }
    }
}
